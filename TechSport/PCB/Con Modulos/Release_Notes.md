## Release Notes ##

### Version 0.2 ###

- **Descripción:** En esta version se crea una pcb mas robusta, con el objetivo de facilitar el soldado manual.  
- **Defectos corregidos:**  
	- Las dimensiones del modulo cargador coinciden casi en su totalidad.  
	- La polaridad del modulo step up esta corregida.  
	- Se elimino la capa de cobre sobrante.  
	- Se ampliaron las superficies de soldadura.  
- **Defectos conocidos:**  
	- Las distancias entre los pines de VSS y B+, como así tambien entre GND y B- en el módulo cargador de batería, difieren en 0.5mm aprox, pero no impide su correcto funcionamiento.  
- **Objetivos de nueva version:** 
	- Agregar un interruptor on/off conectado al modulo 8266.  

-- 

### Version 0.1 ###

- **Descripción:** Version inicial, se agregan los modulos cargador, step up y el modulo 8266. Además se crean pines para conectar el sensor de ultrasonido, el anillo led y la batería.  
- **Defectos conocidos:**  
	- Las dimensiones del módulo cargador no coinciden con las reales.  
	- La polaridad de salida del modulo step up esta invertida.  
	- No se utiliza adecuadamente la capa de cobre como GND.  
	- La superficie de soldado es demasiado pequeña para el soldado manual.  
- **Objetivos de nueva version:**  
	- Crear una pcb con pistas, pads, etc de mayores dimensiones para facilitar el soldado.




