#include "LedsManager.h"

/**
  Inicializa los leds
*/
LedsManager::LedsManager() {
  strip.begin();
  strip.show();
}

/**
  Enciende los leds de acuerdo a la mezcla de RGB
*/
void LedsManager::turnLedsOn(int R, int G, int B) {
  uint32_t color = strip.Color(R, G, B);
  for (uint16_t i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, color);
    delay(1);
  }
  strip.show();
  strip.show();
}

/**
  Apaga los leds y chequea bateria
*/
void LedsManager::turnLedsOff() {
  for (uint16_t i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(0, 0, 0));
    delay(1);
  }
  strip.show(); 
  strip.show();
  checkBattery();
}

/**
  Apaga los leds
*/
void LedsManager::justTurnLedsOff() {
  for (uint16_t i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(0, 0, 0));
    delay(1);
  }
  strip.show(); 
}

/**
  Realiza un giro de 1 segundo
*/
void LedsManager::turnLedsOnCircle(int R, int G, int B) {
  uint32_t color = strip.Color(R, G, B);
  strip.setPixelColor(0, color);
  strip.show();
  delay(24);
  for (uint16_t i = 1; i < strip.numPixels(); i++) {
    strip.setPixelColor(i - 1, strip.Color(0, 0, 0));
    strip.setPixelColor(i, color);
    delay(24);
    strip.show();
  }
  strip.setPixelColor(strip.numPixels() - 1, strip.Color(0, 0, 0));
}

/**
* Realiza un loading circular de 2 segundos
*/
void LedsManager::turnLedsLoading(){
  uint32_t color = strip.Color(0, 80, 0);
  for (uint16_t i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, color);
    strip.show();
    delay(80);
  }
  turnLedsOff();
}

/**
 * Realiza una secuencia de prendido y apagado para notificar bateria baja
 */
void LedsManager::turnOnLowBatterySequence(){
  uint32_t color = strip.Color(15, 3, 0);
  for(uint16_t j = 0; j<4; j++){
    for (uint16_t i = j; i < strip.numPixels(); i+=6) {
      strip.setPixelColor(i, color);
      strip.setPixelColor(i + 1, color);
      delay(1);
    }
    strip.show();
    delay(1000);
    if(j < 3){
      justTurnLedsOff();
      delay(1000);
    }
  }
}
     
/**
 * Chequea estado de bateria y sino apaga el dispositivo
 */
void LedsManager::checkBattery(){
  int batteryStatus= analogRead(A0);
  if(batteryStatus < 793){ //911 = 3.6v, 793 = 3.2v
    for (int i = 0; i < 4; i++){
      int batteryStatus= analogRead(A0);
      if(batteryStatus > 793){
        return;
      }
      delay(50);
    }
    turnOnLowBatterySequence();
    ESP.deepSleep(0);
  }
}


