#include "SensorManager.h"

/**
  Inicializa el sensor
*/
SensorManager::SensorManager() {
  pinMode(TRIGGER, OUTPUT);
  pinMode(ECHO, INPUT);
}

/**
  Retorna la distancia tomada por el sensor
  @return distancia en cm
*/
int SensorManager::getDistance() {
  int results[MEASURES];
  long duration;
  for (int i = 0; i < MEASURES; i++) {
    digitalWrite(TRIGGER, LOW);
    delayMicroseconds(2);

    digitalWrite(TRIGGER, HIGH);
    delayMicroseconds(10);

    digitalWrite(TRIGGER, LOW);
    duration = pulseIn(ECHO, HIGH);
    results[i] = (duration / 2) / 29.1;
    delay(1);
  }
  int distance = detectOutliers(results);
  DEBUG_MSG("[Sensor] Distancia: " + String(distance));

  return distance;

}

int SensorManager::detectOutliers(int results[]) {
  int sum = 0;
  int average = 0;
  int varianceAverage = 0;
  int valid_measures = 0;
  int variances[MEASURES];

  for (int i = 0; i < MEASURES; i++) {
    average += results[i];
  }
  average = average / MEASURES;

  for (int i = 0; i < MEASURES; i++) {
    variances[i] = abs(average - results[i]);
    varianceAverage += variances[i];
  }
  varianceAverage = varianceAverage / MEASURES;

  for (int i = 0; i <= MEASURES; i++) {
    if (variances[i] <= varianceAverage) {
      sum += results[i];
      valid_measures++;
    }
  }
  return sum / valid_measures;
}

