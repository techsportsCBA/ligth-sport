/**
 * Project Untitled
 */

#ifndef _SENSORMANAGER_H
#define _SENSORMANAGER_H

#include <ESP8266WiFi.h>

#define DEBUG 0 //Activa o desactiva mensajes de debug

#if DEBUG
#define DEBUG_MSG(word) Serial.println(word);
#else
#define DEBUG_MSG(word);
#endif

#define MEASURES 4
#define TRIGGER D2
#define ECHO    D1


class SensorManager {
public: 
    
    /**
     * Inicializa el sensor
     */
    SensorManager();
    
    /**
     * Retorna la distancia tomada por el sensor
     * @return distancia en cm
     */
    int getDistance();
    
private: 

    int detectOutliers(int[]);
};

#endif //_SENSORMANAGER_H
