/**
 * Project Untitled
 */

#ifndef _LEDSMANAGER_H
#define _LEDSMANAGER_H

#include <Adafruit_NeoPixel.h>
#include <ESP8266WiFi.h>

#define PIN D3
#define NUMPIXELS      24

class LedsManager {
public: 
    
    /**
     * Inicializa los leds
     */
    LedsManager();

    /**
     * Enciende los leds de acuerdo a la mezcla de RGB
     */
    void turnLedsOn(int R, int G, int B);
    
    /**
     * Apaga los leds y chequea bateria
     */
    void turnLedsOff();
    
    /**
     * Apaga los leds
     */
    void justTurnLedsOff();
    
    /**
     * Realiza un giro de 1 segundo
     */
    void turnLedsOnCircle(int R, int G, int B);

    /**
     * Realiza un giro de 1 segundo por pasos
     */
    void turnLedsOnCircleStep(int R, int G, int B);

    /**
     * Realiza un loading circular de 2 segundos
     */
    void turnLedsLoading();
        
    /**
     * Realiza una secuencia de prendido y apagado para notificar bateria baja
     */
    void turnOnLowBatterySequence();
         
    /**
     * Chequea estado de bateria y sino apaga el dispositivo
     */
    void checkBattery();
    
private:
    Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ400);
    int nLed= 0;
};

#endif //_LEDSMANAGER_H
