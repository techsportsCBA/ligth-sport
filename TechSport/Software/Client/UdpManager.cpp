/**
   Project Untitled
*/


#include "UdpManager.h"

/**
   UdpManager implementation
*/


/**
   Envía un mensaje a la dirección ip solicitada con el tag
   correspondiente para indicar el tipo de mensaje
   @param ip Ip de destino
   @param tag Tipo de mensaje
   @param data Mensaje
   @return 0 si no se recibio ack, 1 si se recibio el ack
*/
int UdpManager::sendData(IPAddress *ip, String *tag, String *data) {
  String packet = *tag + " " + *data;
  IPAddress ipReceived;
  String tagReceived;
  String dataReceived;
  DEBUG_MSG("[UDP] Enviando " + packet + " a la ip " + ip->toString());
  udp.beginPacket(*ip, port);
  udp.write(packet.c_str());
  int i = 0;
  while (!udp.endPacket() && i < 3) {
    udp.beginPacket(*ip, port);
    udp.write(packet.c_str());
    i++;
    delay(1);
  }

  int count = 0;
  int packetSize;

  while (count < timeOut) {
    packetSize = udp.parsePacket();

    if (packetSize)
    {
      int len = udp.read(incomingPacket, 255);
      if (len > 0)
      {
        incomingPacket[len] = 0;
      }
      parseMessage(incomingPacket, &tagReceived, &dataReceived, len);

      if (!strcmp("ack", tagReceived.c_str()) && *ip == udp.remoteIP()) {
        return 1;
      } else {
        DEBUG_MSG("[UDP] Warning: Esperaba un ack y recibí un " + tagReceived + " de la IP: " + udp.remoteIP().toString());
      }
    }
    count++;
    delay(1);
  }
  DEBUG_MSG("[UDP] Error: ACK TimeOut");

  return 0;
}

/**
  Envía un mensaje a la dirección ip solicitada con el tag
  correspondiente para indicar el tipo de mensaje
  @param ip Ip de destino
  @param tag Tipo de mensaje
  @param data Mensaje
  @param port Puerto del destinatario
  @return 0 si no se recibio ack, 1 si se recibio el ack
*/
int UdpManager::sendData(IPAddress *ip, String *tag, String *data, int port) {
  String packet = *tag + " " + *data;
  IPAddress ipReceived;
  String tagReceived;
  String dataReceived;

  for (int i = 0; i < 2; i++) {
    udp.beginPacket(*ip, port);
    udp.write(packet.c_str());
    udp.endPacket();
    delay(10);
  }
  return 0;
}

/**
   Espera un tiempo (timeOut) la recepción de un mensaje
   en el puerto UDP predefinido.
   @param ip Ip de origen del mensaje
   @param tag Tipo de mensaje
   @param data Mensaje
   @return 0 si no se recibio nada, 1 si se recibio un mensaje
*/
int UdpManager::receiveData(IPAddress *ip, String *tag, String *data) {

  //int count = 0;
  int packetSize;

  //while (count < timeOut) {
  packetSize = udp.parsePacket();

  if (packetSize)
  {
    int len = udp.read(incomingPacket, 255);
    if (len > 0)
    {
      incomingPacket[len] = 0;
    }
    parseMessage(incomingPacket, tag, data, len);
    *ip = udp.remoteIP();

    DEBUG_MSG("[UDP] Recibí de la IP: " + udp.remoteIP().toString() + ":\nTAG: " + *tag + "\nDATA: " + *data);

    if (*tag != String("ack")) {
      for (int i = 0; i < 2; i++) {
        delay(random(10));
        udp.beginPacket(udp.remoteIP(), port);
        udp.write(ack);
        udp.endPacket();
      }
      return 1;
    }
  }
  //count++;
  //delay(1);
  //}
  return 0;
}

/**
   Envía multiples mensajes a diferentes direcciones ip
   al mismo tiempo y luego espera los ack.
   @param ip Array de Ips de destino
   @param tag Array de Tipos de mensajes(si es "none" no se enviara el mensaje)
   @param data Array de Mensajes
   @param len Cantidad de mensajes a enviar
   @param isReceived Array de enteros que determina cual mensaje se recibio con exito
   @return 0 si no se recibio algun ack, 1 si se recibio el ack
*/
int UdpManager::sendDataMultiCast(IPAddress *ip, String *tag, String *data, int len, int *isReceived) {
  String packet;
  IPAddress ipReceived;
  String tagReceived;
  String dataReceived;
  int targets = 0;

  for (int i = 0; i < len; i++) {
    isReceived[i] = 0;
    if (tag[i] != "none") {
      packet = *(tag + i) + " " + *(data + i);
      udp.beginPacket(*(ip + i), port);
      udp.write(packet.c_str());
      while (!udp.endPacket()) {
        udp.beginPacket(*(ip + i), port);
        udp.write(packet.c_str());
        delay(1);
      }
      targets++;
    } else {
      isReceived[i] = -1;
    }
  }

  int count = 0;
  int packetSize;
  int i = 0;

  while (count < timeOut) {
    packetSize = udp.parsePacket();
    if (packetSize)
    {
      int len = udp.read(incomingPacket, 255);
      if (len > 0)
      {
        incomingPacket[len] = 0;
      }
      parseMessage(incomingPacket, &tagReceived, &dataReceived, len);

      if (!strcmp("ack", tagReceived.c_str())) {
        for ( int j = 0; j < len; j++) {
          if (ip[j] == udp.remoteIP() && !isReceived[j]) {
            isReceived[j] = 1;
            i++;
            if ( i == targets) {
              return 1;
            }
            break;
          } else {
            DEBUG_MSG("[UDP] Warning: Ya registre el ACK de la IP: " + udp.remoteIP().toString());
          }
        }
      } else {
        DEBUG_MSG("[UDP] Warning: Esperaba un ack y recibí un " + tagReceived + " de la IP: " + udp.remoteIP().toString());
      }
    }
    count++;
    delay(1);
  }
  DEBUG_MSG("[UDP] Error: ACK TimeOut");
  return 0;
}

/**
  Abre el puerto UDP predefinido
  @return 0 si no se pudo abrir, 1 si se abrio el puerto
*/
int UdpManager::open() {
  if (udp.begin(port)) {
    DEBUG_MSG("[UDP] Puerto UDP abierto en " + String(port));
    return 1;
  }
  else {
    DEBUG_MSG("[UDP] Error: No se pudo abrir el puerto UDP");
  }
  return 0;
}


int UdpManager::open(int port) {
  this->port = port;
  return open();
}

/**
  Elimina datos guardados en el buffer
*/
void UdpManager::clearBuffer() {
  int packetSize;
  while (packetSize = udp.parsePacket()) {
    udp.flush();
  }
}

/**
  Setea el timeOut de los ACK
  @param timeOut
*/
void UdpManager::setTimeOut(int timeOutAck) {
  timeOut = timeOutAck;
}

/**
  Retorna el timeOut de los ACK
  @return timeOut
*/
int UdpManager::getTimeOut() {
  return timeOut;
}


int UdpManager::parseMessage(char *packet, String *tag, String *data, int len) {
  char tagTemp[20];

  int i = 0;

  while ( i < sizeof(tagTemp)  && i < len) {
    tagTemp[i] = packet[i];
    if (packet[i] == ' ') {
      tagTemp[i] = '\0';
      *tag = tagTemp;
      *data = packet + i + 1;
      return 1;
    }
    i++;
  }
  tagTemp[i] = '\0';
  *tag = tagTemp;
  return 0;
}

