/**
 * Project Untitled
 */


#ifndef _GAMECLIENT_H
#define _GAMECLIENT_H

#include <Arduino.h>
#include "UdpManager.h"
#include "LedsManager.h"
#include "SensorManager.h"

#define DEBUG 1 //Activa o desactiva mensajes de debug

#if DEBUG
#define DEBUG_MSG(word) Serial.println(word);
#else
#define DEBUG_MSG(word);
#endif

#define TTL_MAX 5 //Cantidad de intentos de enviar un mensaje


class GameClient {
public: 
    
    /**
     * Inicializa el cliente
     */
    GameClient(void);

    /**
     * Registra el nodo en el servidor
     * @return 1 si lo registro, 0 si no lo hizo
     */
    int registerNode();

    /**
     * Espera recibir un comando del servidor
     * @return 1 si lo recibio, 0 si no lo hizo
     */
    int listen();

    /**
     * Registra datos del sensor y envía al servidor
     * un aviso, si se selecciono el nodo.
     * @return 1 si envió el aviso, 0 si no lo hizo
     */
    int manageNode();
    
private: 
    UdpManager udp;
    IPAddress serverIP;
    SensorManager sensorManager;
    LedsManager ledsManager;
    int R, G, B;
    boolean isMyTurn = false;
    int sensorOn = 0;
    int loading =0;

    /**
     * Divide al string según el delimitador
     * @param *string cadena a dividir
     * @param delimiter delimitador
     * @return cadena dividida
     */
    String splitString(String *string, String delimiter);

    /**
     * Envía al servidor un mensaje
     * @param tag Tipo de mensaje
     * @param data Mensaje
     * @return 1 si el servidor lo recibió, 0 si no lo hizo
     */
    int sendToServer(String *tag, String *data);

    /**
     * Actualiza el turno y el color de los leds
     * @param data Mensaje del servidor
     */
    void refreshNode(String data);

    /**
     * Avisa al servidor que el nodo fue seleccioado
     * @return 1 si el servidor recibio el aviso, 0 lo contrario
     */
    int nodeTouched();
};

#endif //_GAMECLIENT_H
