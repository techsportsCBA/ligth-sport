/**
 * Project Untitled
 */


#ifndef _UDPMANAGER_H
#define _UDPMANAGER_H

#define DEBUG 0 //Activa o desactiva mensajes de debug

#if DEBUG
#define DEBUG_MSG(word) Serial.println(word);
#else
#define DEBUG_MSG(word);
#endif

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

class UdpManager {
public: 
    
    /**
     * Envía un mensaje a la dirección ip solicitada con el tag 
     * correspondiente para indicar el tipo de mensaje
     * @param ip Ip de destino
     * @param tag Tipo de mensaje
     * @param data Mensaje
     * @return 0 si no se recibio ack, 1 si se recibio el ack
     */
    int sendData(IPAddress *ip, String *tag, String *data);

    /**
     * Envía un mensaje a la dirección ip solicitada con el tag 
     * correspondiente para indicar el tipo de mensaje
     * @param ip Ip de destino
     * @param tag Tipo de mensaje
     * @param data Mensaje
     * @param port Puerto del destinatario
     * @return 0 si no se recibio ack, 1 si se recibio el ack
     */
    int sendData(IPAddress *ip, String *tag, String *data, int port);
    
    /**
     * Espera un tiempo (timeOut) la recepción de un mensaje
     * en el puerto UDP predefinido.
     * @param ip Ip de origen del mensaje
     * @param tag Tipo de mensaje
     * @param data Mensaje
     * @return 0 si no se recibio nada, 1 si se recibio un mensaje
     */
    int receiveData(IPAddress *ip, String *tag, String *data);

    /**
     * Envía multiples mensajes a diferentes direcciones ip
     * al mismo tiempo y luego espera los ack.
     * @param ip Array de Ips de destino
     * @param tag Array de Tipos de mensajes(si es "none" no se enviara el mensaje)
     * @param data Array de Mensajes
     * @param len Cantidad de mensajes a enviar
     * @param isReceived Array de enteros que determina cual mensaje se recibio con exito
     * @return 0 si no se recibio algun ack, 1 si se recibio el ack
     */
    int sendDataMultiCast(IPAddress *ip, String *tag, String *data, int len, int *isReceived);

    /**
     * Abre el puerto UDP predefinido
     * @return 0 si no se pudo abrir, 1 si se abrio el puerto
     */
    int open();

    /**
     * Abre el puerto UDP "port"
     * @return 0 si no se pudo abrir, 1 si se abrio el puerto
     */
    int open(int port);

    /**
     * Elimina datos guardados en el buffer
     */
    void clearBuffer();

    /**
     * Setea el timeOut de los ACK
     * @param timeOut
     */
    void setTimeOut(int timeOutAck);

    /**
    * Retorna el timeOut de los ACK
    * @return timeOut
    */
    int getTimeOut();
    
private: 
    WiFiUDP udp;
    unsigned int port = 678;
    const char* ack = "ack";
    int timeOut = 200;
    char incomingPacket[255];

    int parseMessage(char *packet, String *tag, String *data, int len);
    
};

#endif //_UDPMANAGER_H
