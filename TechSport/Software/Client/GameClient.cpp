#include "GameClient.h"

/**
  Inicializa el cliente
*/
GameClient::GameClient() {
  serverIP = IPAddress(192, 168, 4, 1);
}

/**
  Registra el nodo en el servidor
  @return 1 si lo registro, 0 si no lo hizo
*/
int GameClient::registerNode() {
  ledsManager.turnLedsOff();
  if (udp.open()) {
    DEBUG_MSG("[Game] --- Registrando nodo ---");
    String tag = "register";
    String data = "message";

    int response = sendToServer(&tag, &data);
    if (response) {
      ledsManager.turnLedsOn(0, 100, 0);
      delay(3000);
      ledsManager.turnLedsOff();
      DEBUG_MSG("[Game] Registrado!");
    } else {
      ledsManager.turnLedsOn(100, 0, 0);
      delay(3000);
      ledsManager.turnLedsOff();
      DEBUG_MSG("[Game] Error: No se pudo registrar");
    }
    DEBUG_MSG("[Game] --- Fin Envío de Registro ---");
    return response;
  } else {
    DEBUG_MSG("[Game] Error: No se pudo abrir el puerto UDP");
  }
  return 0;
}

/**
  Avisa al servidor que el nodo fue seleccioado
  @return 1 si el servidor recibio el aviso, 0 lo contrario
*/
int GameClient::nodeTouched() {
  ledsManager.turnLedsOff();
  DEBUG_MSG("[Game] --- Nodo tocado, enviando informacion al servidor ---");
  String tag = "selected";
  String data = " ";
  int response = sendToServer(&tag, &data);
  if (response) {
    DEBUG_MSG("[Game] Enviado Correctamente");
  } else {
    DEBUG_MSG("[Game] No se pudo enviar");
  }
  isMyTurn = false;
  sensorOn = 0;
  DEBUG_MSG("[Game] --- Fin Envío de informacion ---");
  return response;
}

/**
  Envía al servidor un mensaje
  @param tag Tipo de mensaje
  @param data Mensaje
  @return 1 si el servidor lo recibió, 0 si no lo hizo
*/
int GameClient::sendToServer(String *tag, String *data) {
  int ret = 1;
  if (WiFi.status() == WL_CONNECTED) {
    for (int ttl = 0; ttl < TTL_MAX; ttl++) {
      if (udp.sendData(&serverIP, tag, data)) {
        DEBUG_MSG("[Game] --> √ Recibido!");
        break;
      } else if ( ttl == TTL_MAX - 1) {
        DEBUG_MSG("[Game] --> X No Recibido!");
        ret = 0;
      }
    }
  }
  return ret;
}

/**
  Actualiza el turno y el color de los leds
  @param data Mensaje del servidor
*/
void GameClient::refreshNode(String data) {
  isMyTurn = splitString(&data, " ").toInt();
  R = splitString(&data, " ").toInt();
  G = splitString(&data, " ").toInt();
  B = data.toInt();
  if ((R == 0) && (B == 0) && (G == 0)) {
    sensorOn = 0;
  }
  ledsManager.turnLedsOn(R, G, B);
}

/**
  Registra datos del sensor y envía al servidor
  un aviso, si se selecciono el nodo.
  @return 1 si envió el aviso, 0 si no lo hizo
*/
int GameClient::manageNode() {
  if (sensorOn) {
    if ((sensorManager.getDistance() <= 20) && (sensorManager.getDistance()) ) {
      return nodeTouched();
    }
  }
  return 0;
}

/**
  Divide al string según el delimitador
  @param *string cadena a dividir
  @param delimiter delimitador
  @return cadena dividida
*/
String GameClient::splitString(String *string, String delimiter) {
  String response;
  for (int i = 0; i < string->length(); i++) {
    if (string->substring(i, i + 1) == delimiter) {
      response = string->substring(0, i);
      *string = string->substring(i + 1);
      break;
    }
  }
  return response;
}


/**
  Espera recibir un comando del servidor
  @return 1 si lo recibio, 0 si no lo hizo
*/
int GameClient::listen() {
  String tag, data;
  IPAddress ip;
  if (udp.receiveData(&ip, &tag, &data)) {
    DEBUG_MSG("[Game] Recibí tag: " + tag + ", data: " + data);
    if (!strcmp("clear", tag.c_str())) {
      //turn leds and sensor off
      DEBUG_MSG("[Game] Apagando sensor y leds");
      ledsManager.turnLedsOff();
      isMyTurn = false;
      sensorOn = 0;
      loading = 0;
    } else if ((!strcmp("game", tag.c_str())) && (!sensorOn)) {
      //check if must turn on lights
      sensorOn = 1;
      DEBUG_MSG("[Game] Encendiendo sensor y leds");
      refreshNode(data);
    } else if (!strcmp("loading", tag.c_str())) {
      if (!loading) {
        loading = 1;
        ledsManager.turnLedsLoading();
      }
    } /*else {
      String tag = "ack";
      String data = "";
      sendToServer(&tag, &data);
    }*/
    return 1;
  }
  return 0;
}


