package com.kalantos.techsport.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.util.Log;
import com.kalantos.techsport.R;
import com.kalantos.techsport.activities.MainActivity;
import com.kalantos.techsport.utils.BluetoothConnectionService;
import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 */
public class BluetoothFragment extends Fragment{

    private static final String TAG = "BluetoothFragment";
    private static final UUID MY_UUID_INSECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    BluetoothAdapter mBluetoothAdapter;
    BluetoothConnectionService mBluetoothConnection;
    BluetoothDevice mBTDevice;
    boolean activeBluetoothFragment;
    int timeout;

    public BluetoothFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bluetooth, container, false);
        return view;
    }

    @Override
    public void onStart(){
        super.onStart();
        activeBluetoothFragment = true;
        timeout = 0;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        enableBluetooth();
        Thread discoverThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(activeBluetoothFragment){
                    try {
                        discover();
                        Thread.sleep(4000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    timeout++;
                    if(timeout == 5 && activeBluetoothFragment){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((MainActivity) getActivity()).displayToast( "Servidor no encontrado, por favor enciendalo");
                            }
                        });
                    }

                }
            }
        });
        discoverThread.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            getActivity().unregisterReceiver(mBroadcastReceiverDiscover);
        }catch (Exception e){
            Log.w(TAG, "reciever unregister twice");
        }
        activeBluetoothFragment = false;
        mBluetoothAdapter.cancelDiscovery();
    }

    /**
     * Broadcast Receiver para detectar dispositivos encontrados
     */
    public BroadcastReceiver mBroadcastReceiverDiscover = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();

        if (action.equals(BluetoothDevice.ACTION_FOUND)){
            BluetoothDevice device = intent.getParcelableExtra (BluetoothDevice.EXTRA_DEVICE);
            if(device.getName() == null || device.getName().equals("null") || device.getName().equals("HC-05")){
                bondAndConnect(device);
            }else{
                Log.w(TAG,"FOUND_DEV: " + device.getName());
            }
        }
        }
    };

    /**
     * Enciende el bluetooth si este no esta prendido
     */
    public void enableBluetooth(){
        if(mBluetoothAdapter == null){
            Log.e(TAG, "Device dont have bluetooth");
        }
        if(!mBluetoothAdapter.isEnabled()){
            Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableBTIntent);
        }
    }

    /**
     * Comienza a buscar dispositivos bluetooth
     */
    public void discover() {

        checkBTPermissions();
        if(mBluetoothAdapter.isDiscovering()){
            mBluetoothAdapter.cancelDiscovery();
            mBluetoothAdapter.startDiscovery();
            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            getActivity().registerReceiver(mBroadcastReceiverDiscover, discoverDevicesIntent);
        }
        if(!mBluetoothAdapter.isDiscovering()){
            mBluetoothAdapter.startDiscovery();
            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            getActivity().registerReceiver(mBroadcastReceiverDiscover, discoverDevicesIntent);
        }
    }

    /**
     *  Chequea los permisos para android superior a marshmallow
     *
     */
    private void checkBTPermissions() {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.M){
            int permissionCheck = getActivity().checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += getActivity().checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {
                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
            }
        }
    }


    private void bondAndConnect( BluetoothDevice device){
        mBluetoothAdapter.cancelDiscovery();

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2){
            mBTDevice = device;
            mBluetoothConnection = new BluetoothConnectionService(getActivity());
            mBluetoothConnection.startClient(mBTDevice,MY_UUID_INSECURE );

            activeBluetoothFragment = false;
            ((MainActivity)getActivity()).setBluetoothService(mBluetoothConnection);
            int errorLogCount = 0;
            while(!mBluetoothConnection.getConnectedThreadStatus()){
                errorLogCount++;
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(errorLogCount >= 750){
                    Log.e(TAG, "Error starting ConnectedThread");
                }
            }
            ((MainActivity)getActivity()).checkThread();
            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
        }

    }
}
