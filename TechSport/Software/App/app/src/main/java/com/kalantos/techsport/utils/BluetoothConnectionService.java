package com.kalantos.techsport.utils;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;

import com.kalantos.techsport.activities.MainActivity;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.UUID;


public class BluetoothConnectionService implements Serializable {

    private static final String TAG = "BluetoothService";
    private static final long serialVersionUID = -7060210544600464481L;
    private final BluetoothAdapter mBluetoothAdapter;
    private ConnectThread mConnectThread;
    private BluetoothDevice mmDevice;
    private UUID deviceUUID;
    private ConnectedThread mConnectedThread;
    MainActivity mainActivity;
    boolean ConnectedThreadStarted;

    public BluetoothConnectionService(Context context) {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.mainActivity = (MainActivity) context;
    }

    /**
    * Conecta con un device y un uuid
     **/
    public void startClient(BluetoothDevice device,UUID uuid){

        mConnectThread = new ConnectThread(device, uuid);
        mConnectThread.start();
    }

    /**
     * Inicia un hilo (connected) para recibir los mensajes del HC-05
     * */
    private void connected(BluetoothSocket mmSocket) {

        mConnectedThread = new ConnectedThread(mmSocket);
        mConnectedThread.start();
    }

    /**
     * Envia un dato a la HC-05 usando el hilo (connected)
     */
    public boolean write(byte[] out) {
       return mConnectedThread.write(out);
    }

    public boolean getConnectedThreadStatus(){
        return ConnectedThreadStarted;
    }

    /**
     * Hilo que se encarga de la conexion al modulo HC-05
     */
    private class ConnectThread extends Thread implements Serializable {
        private BluetoothSocket mmSocket;

        public ConnectThread(BluetoothDevice device, UUID uuid) {
            mmDevice = device;
            deviceUUID = uuid;
        }

        public void run(){
            BluetoothSocket tmp = null;
            try {
                tmp = mmDevice.createRfcommSocketToServiceRecord(deviceUUID);
            } catch (IOException e) {
                Log.d(TAG, "Error al crear el socket bluetoot");
               //Toast.makeText(this, "Error al crear el socket bluetooth",Toast.LENGTH_SHORT).show();
            }

            mmSocket = tmp;
            mBluetoothAdapter.cancelDiscovery();

            try {
                mmSocket.connect();
            } catch (IOException e) {
                try {
                    Log.d(TAG, "Exception: Closing Socket.");
                    mmSocket.close();
                } catch (IOException e1) {
                    Log.e(TAG, "Exception: Unable to close connection in socket " + e1.getMessage());
                }
            }
            connected(mmSocket);
        }
        public void cancel() {
            try {
                Log.d(TAG, "cancel: Closing Client Socket.");
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "cancel: close() of mmSocket in Connectthread failed. " + e.getMessage());
            }
        }
    }

    /**
     * Hilo que se encarga del envio y recepcion de mensajes al modulo HC-05
     **/
    private class ConnectedThread extends Thread implements Serializable{
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "ConnectedThread: Starting.");

            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = mmSocket.getInputStream();
                tmpOut = mmSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
            ConnectedThreadStarted = true;
        }

        public void run(){
            byte[] buffer = new byte[1024];  // buffer store for the stream

            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                // Read from the InputStream
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    bytes = mmInStream.read(buffer);
                    String incomingMessage = new String(buffer, 0, bytes);
                    //Log.d(TAG, "InputStream: " + incomingMessage);
                    mainActivity.ConcatBluetoothMessage(incomingMessage);
                } catch (IOException e) {
                    Log.e(TAG, "write: Error reading Input Stream. " + e.getMessage() );
                    break;
                }
            }
        }

        //Call this from the main activity to send data to the remote device
        public boolean write(byte[] bytes) {
            String text = new String(bytes, Charset.defaultCharset());
            //Log.d(TAG, "write: Writing to outputstream: " + text);
            try {
                mmOutStream.write(bytes);
                return true;
            } catch (IOException e) {
                Log.e(TAG, "write: Error writing to output stream. " + e.getMessage() );
                ConnectedThreadStarted = false;
                return false;
            }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }

}
























