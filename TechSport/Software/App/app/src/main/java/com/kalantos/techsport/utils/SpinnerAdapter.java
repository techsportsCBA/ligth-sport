package com.kalantos.techsport.utils;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kalantos.techsport.R;

import java.util.ArrayList;

public class SpinnerAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> listValues;
    LayoutInflater inflter;

    public SpinnerAdapter(Context applicationContext, ArrayList<String> listValues) {
        this.context = applicationContext;
        this.listValues = listValues;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return listValues.size();
    }

    @Override
    public Object getItem(int i) {
        return listValues.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = super.getDropDownView(position,convertView,parent);
        TextView dropdown = (TextView)view.findViewById(R.id.spinnerTextView);
        dropdown.setTextColor(Color.BLACK);
        dropdown.setGravity(Gravity.LEFT);
        dropdown.setPadding(20,20,20,20);
        return view;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_item, null);
        TextView textView = (TextView) view.findViewById(R.id.spinnerTextView);
        textView.setText(listValues.get(i));
        return view;
    }
}