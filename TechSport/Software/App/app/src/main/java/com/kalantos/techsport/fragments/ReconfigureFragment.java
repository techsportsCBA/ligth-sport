package com.kalantos.techsport.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kalantos.techsport.R;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReconfigureFragment extends Fragment {


    public ReconfigureFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reconfigure, container, false);
    }
    @Override
    public void onStart() {
        super.onStart();
        setFinalResults();
    }
    private void setFinalResults() {

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("config_values",
                Context.MODE_PRIVATE);
        boolean gameBreak =  sharedPreferences.getBoolean("gameBreak", false);
        int average = 0;
        int min = 0;
        int max = 0;
        int correctTouchs = 1;
        int totalTouchs = 1;
        int correctTouchsPercentage = 0;
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            average = bundle.getInt("average", 0);
            min = bundle.getInt("min", 0);
            max = bundle.getInt("max", 0);
            correctTouchs = bundle.getInt("correctTouchs", 1);
            if(gameBreak){
                totalTouchs = sharedPreferences.getInt("touchs", 10);
            }else {
                totalTouchs = bundle.getInt("totalTouchs", 1);
            }
            correctTouchsPercentage =(100 * correctTouchs) / totalTouchs;
        }

        TextView averageTextview = (TextView) getView().findViewById(R.id.reconfigure_averagevalue_textview);
        TextView minTextview = (TextView) getView().findViewById(R.id.reconfigure_minvalue_textview);
        TextView maxTextview = (TextView) getView().findViewById(R.id.reconfigure_maxvalue_textview);
        TextView correctTouchsPercentageTextview = (TextView) getView().findViewById(R.id.reconfigure_correcttouchs_textview);
        TextView correctVsTotalTouchsTextview = (TextView) getView().findViewById(R.id.reconfigure_correctvstotaltouchs_textview);

        String averageString = fromMilisecondsToSecondsString(average);
        String minString = fromMilisecondsToSecondsString(min);
        String maxString = fromMilisecondsToSecondsString(max);

        averageTextview.setText(averageString);
        minTextview.setText(minString);
        maxTextview.setText(maxString);
        String correctPercentageString = getString(R.string.correctPercentage);
        String correctVsTotalTouchsString = "("+correctTouchs+"/"+totalTouchs+")";
        correctTouchsPercentageTextview.setText(correctTouchsPercentage + correctPercentageString);
        correctVsTotalTouchsTextview.setText(correctVsTotalTouchsString);
    }
    private String fromMilisecondsToSecondsString(int milis){
        if(milis == -100){
            return "x";
        }

        float seconds = (float) milis / 1000;
        return String.format("%.3f s", seconds);
    }
}
