package com.kalantos.techsport.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import com.kalantos.techsport.R;
import com.kalantos.techsport.activities.MainActivity;
import com.kalantos.techsport.utils.SpinnerAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfigureFragment extends Fragment {

    ArrayList<String> gameModeArray;
    ArrayList<String> lightColorsArray;

    public ConfigureFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fillSpinnerArrays();
        return inflater.inflate(R.layout.fragment_configure, container, false);
    }

    private void fillSpinnerArrays(){
        gameModeArray = new ArrayList<>();
        gameModeArray.add("Simple");
        gameModeArray.add("Simple con erroneas");
        gameModeArray.add("Complejo");
        gameModeArray.add("Complejo aleatorio");
        gameModeArray.add("Punto fijo");
        gameModeArray.add("Simon dice");

        lightColorsArray = new ArrayList<>();
        lightColorsArray.add("Verde");
        lightColorsArray.add("Rojo");
        lightColorsArray.add("Amarillo");
        lightColorsArray.add("Azul");
        lightColorsArray.add("Violeta");
    }

    @Override
    public void onStart(){
        Button sendButton = (Button) getView().findViewById(R.id.configure_send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                send();
            }
        });
        ImageView refillButton = (ImageView) getActivity().findViewById(R.id.configure_fillconfig_imageview);
        ImageView settingsButton = (ImageView) getActivity().findViewById(R.id.configure_settings_imageview);
        refillButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                fillEditText();
            }
        });
        settingsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                transactionToSettings();
            }
        });

        Spinner gameModeSpinner = (Spinner) getActivity().findViewById(R.id.configure_gamemode_spinner);
        Spinner lightColorsSpinner = (Spinner) getActivity().findViewById(R.id.configure_lightcolors_spinner);
        SpinnerAdapter gameModeAdapter = new SpinnerAdapter(getActivity(),gameModeArray);
        SpinnerAdapter lightColorsAdapter = new SpinnerAdapter(getActivity(),lightColorsArray);
        gameModeSpinner.setAdapter(gameModeAdapter);
        lightColorsSpinner.setAdapter(lightColorsAdapter);
        super.onStart();

    }

    private void saveLastConfiguration(String playerName,int touchs, String lightsColor, int gameMode){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("config_values",
                Context.MODE_PRIVATE);
        sharedPreferences.edit().putInt("touchs", touchs)
                .putString("lightsColor", lightsColor)
                .putString("playerName",playerName)
                .putInt("gameMode", gameMode)
                .commit();
    }

    private void send() {
        Map<String, String> spinnerHash = getColorRGBHashMap();
        EditText editTouches = (EditText) getActivity().findViewById(R.id.configure_touchs_textview);
        EditText editPlayerName = (EditText) getActivity().findViewById(R.id.configure_playername_textview);
        Spinner ligthColorsSpinner = (Spinner) getActivity().findViewById(R.id.configure_lightcolors_spinner);
        Spinner gameModeSpinner = (Spinner) getActivity().findViewById(R.id.configure_gamemode_spinner);

        int gameModeValue = gameModeArray.indexOf(gameModeSpinner.getSelectedItem().toString());
        String color = ligthColorsSpinner.getSelectedItem().toString();
        String RGBColor = spinnerHash.get(color);
        String playerName = editPlayerName.getText().toString();
        String stringTouches = editTouches.getText().toString();

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("config_values",
                Context.MODE_PRIVATE);
        int timeOn =  sharedPreferences.getInt("timeOn", 15);
        int timeOff =  sharedPreferences.getInt("timeOff", 5);
        boolean gameBreak =  sharedPreferences.getBoolean("gameBreak", false);
        int gameBreakInt;
        if(gameBreak){
            gameBreakInt = 1;
        }else{
            gameBreakInt = 0;
        }
        if (!stringTouches.equals("") && !playerName.equals("")) {
            int touchs = Integer.parseInt(stringTouches);
            if (checkConfiguration(touchs, timeOn, timeOff)) {
                if(((MainActivity) getActivity()).sendConfiguration(playerName, touchs, timeOn, timeOff, RGBColor, gameModeValue, gameBreakInt)){
                    saveLastConfiguration(playerName ,touchs, RGBColor, gameModeValue);
                    transactionToResults();
                }else{
                    ((MainActivity) getActivity()).displayToast("Servidor no esta listo, espere luz verde");
                }
            }
        }else {
            ((MainActivity) getActivity()).displayToast("Falta llenar algun campo");
        }
    }

    private void transactionToResults(){
        ResultsFragment newFragment = new ResultsFragment();
        FragmentTransaction transaction =getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentHolderMenu, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void transactionToSettings(){
        SettingsFragment newFragment = new SettingsFragment();
        FragmentTransaction transaction =getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentHolderMenu, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private Map<String,String> getColorRGBHashMap(){

        Map<String,String> spinnerHash = new HashMap<String,String>();
        spinnerHash.put("Verde","0 80 0");
        spinnerHash.put("Rojo","80 0 0");
        spinnerHash.put("Amarillo","40 40 0");
        spinnerHash.put("Azul","0 0 80");
        spinnerHash.put("Violeta","40 0 40");
        return spinnerHash;
    }

    private Map<String,Integer> getColorPositionHashMap(){

        Map<String,Integer> spinnerHash = new HashMap<String,Integer>();
        spinnerHash.put("0 80 0", 0);
        spinnerHash.put("80 0 0", 1);
        spinnerHash.put("40 40 0", 2);
        spinnerHash.put("0 0 80", 3);
        spinnerHash.put("40 0 40", 4);
        return spinnerHash;
    }


    public void fillEditText(){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("config_values",
                Context.MODE_PRIVATE);
        int touchs = sharedPreferences.getInt("touchs", 10);
        String playerName = sharedPreferences.getString("playerName","Anonimo");
        int gameMode = sharedPreferences.getInt("gameMode", 0);
        String lightsColor =  sharedPreferences.getString("lightsColor", "0 80 0");
        Map<String, Integer> spinner = getColorPositionHashMap();
        int lightColorsPosition = spinner.get(lightsColor);
        EditText editTouchs = (EditText) getActivity().findViewById(R.id.configure_touchs_textview);
        EditText editPlayerName = (EditText) getActivity().findViewById(R.id.configure_playername_textview);
        Spinner ligthsMainColor = (Spinner) getActivity().findViewById(R.id.configure_lightcolors_spinner);
        Spinner gameModeSpinner = (Spinner) getActivity().findViewById(R.id.configure_gamemode_spinner);

        editPlayerName.setText(playerName);
        editTouchs.setText(touchs + "");
        ligthsMainColor.setSelection(lightColorsPosition);
        gameModeSpinner.setSelection(gameMode);

    }

    private boolean checkConfiguration(int touchs, int timeOn, int timeOff){
        if(touchs < 0 || touchs > 200){
            ((MainActivity) getActivity()).displayToast("Error en el campo touchs");
            return false;
        } else if(timeOn < 0 || timeOn > 200){
            ((MainActivity) getActivity()).displayToast("Error en el campo timeOn");
            return false;
        } else if(timeOff < 0 || timeOff > 200){
            ((MainActivity) getActivity()).displayToast("Error en el campo timeOff");
            return false;
        }
        return true;

    }

}
