package com.kalantos.techsport.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.kalantos.techsport.R;
import com.kalantos.techsport.activities.MainActivity;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResultsFragment extends Fragment {
    Boolean gameIsOn;
    int average, max, min, totalTouchs, correctTouchs;
    Thread refreshResults;
    boolean threadFinish;
    String bluetoothMessage = "";

    public ResultsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_results, container, false);
    }

    @Override
    public void onStart() {
        ((MainActivity) getActivity()).deleteBluetoothMessage();
        startResultsThread();
        super.onStart();
    }

    @Override
    public void onPause(){
        gameIsOn = false;
        ((MainActivity) getActivity()).deleteBluetoothMessage();
        while(!threadFinish);
        super.onPause();
    }

    private void startResultsThread(){
        gameIsOn = true;
        refreshResults = new Thread(new Runnable() {
            @Override
            public void run() {
                while (gameIsOn) {
                    threadFinish = false;
                    Handler mainHandler = new Handler(getActivity().getApplicationContext().getMainLooper());

                    Runnable myRunnable = new Runnable() {
                        @Override
                        public void run() {
                            if(gameIsOn) {
                                String response = ((MainActivity) getActivity()).getBluetoothMessage();
                                if(response.equals(bluetoothMessage)){
                                    int touchValue = getNewTouch(response);
                                    if (touchValue != -1 && touchValue != -50) {
                                        refreshScreen(touchValue);
                                    } else if (touchValue == -50) {
                                        transactionToReconfigure();
                                    }
                                    bluetoothMessage = "";
                                }else{
                                    bluetoothMessage = response;
                                }
                            }
                        }
                    };
                    mainHandler.post(myRunnable);
                    threadFinish = true;
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        refreshResults.start();
    }

    /**
     * Gestiona las acciones a tomar segun el tag que reciba, aplicando chequeos para evitar valores
     *  corruptos.
     * */
    private int getNewTouch(String response) {
        if (!response.equals("")) {
            // Log.d("resultsFragment",response);
            if (getTag(response).equals("results")) {
                //controlo la cantidad de resultados luego del tag results
                ArrayList validValues = fillValuesArray(response);

                if (validValues != null) {
                    Log.d("ResultsFragment", response);
                    ((MainActivity) getActivity()).deleteBluetoothMessage();
                    getFinalValues(validValues);
                    return -50;
                } else {
                    return -1;
                }
            } else if (getTag(response).equals("touch") && getTouchTime(response) != -1) {
                ((MainActivity) getActivity()).deleteBluetoothMessage();
                return getTouchTime(response);
            }else if(response.length() > 20){
                controlBluetoothMessage(response);
            }
        }
        return -1;
    }

    /**
     * Funcion que devuelve el tag del mensaje bluetooth.
     *  Mensaje : "results[TAG] 129[VALOR]"
     * */
    private String getTag(String response){
        try {
            String[] resultsArray = response.split(" ");
            String tag = resultsArray[0];
            tag.toLowerCase();
            tag = regexTagParser(tag,"touch");
            tag = regexTagParser(tag,"results");
            return tag;
        }catch (Exception e){
            return "error";
        }
    }

    /**
     * Funcion que devuelve una lista de valores recibidos en results, y luego espera un tiempo de
     * backoff para asegurarse que es el array final . Ej:
     *  "results 1 2 3 4 5" -> coloca messageConfirmation = true y  bluetoothMessage = "results 1 2 3 4 5"
     *  si el mensaje siguiente es igual es decir no llego nada nuevo despues del tiempo de backoff
     *  devuelve la matriz de valores, sino vuelve a cargar bluetooth message y espera el backoff
     * */
    private ArrayList<Integer> fillValuesArray(String response){
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String newResponse = ((MainActivity) getActivity()).getBluetoothMessage();
        if(newResponse.equals(response)){
            String[] resultsArray = response.split(" ");
            ArrayList values = new ArrayList();
            for (int i = 1; i <  resultsArray.length; i++){
                int result;
                try {
                    result = Integer.parseInt(resultsArray[i]);
                    values.add(result);
                }catch (Exception e){
                    Log.e("RESULTS","El valor " + resultsArray[i] + " fue descartado");
                }
            }
            Log.d("RESULTS","Valores validos: " + values.size());
            return values;
        }
        return null;
    }

    /**
     * Funcion que obtiene el tiempo a partir de una trama valida. Ej:
     *  "touch 132[TIEMPO]"
     * */
    private int getTouchTime(String response){
        try {
            String[] resultsArray = response.split(" ");
            return Integer.parseInt(resultsArray[1]);
        }catch (Exception e){
            return -1;
        }
    }

    /**
     * Funcion que refresca los valores mostrados en pantalla cuando viene una trama
     *  valida. Ej : " touch 230"
     * */
    private void refreshScreen(int touchValue){
        TextView firstResult = (TextView) getActivity().findViewById(R.id.results_main_textview);
        TextView secondResult = (TextView) getActivity().findViewById(R.id.results_second_textview);
        TextView thirdResult = (TextView) getActivity().findViewById(R.id.results_third_textview);
        TextView fourthResult = (TextView) getActivity().findViewById(R.id.results_fourth_textview);

        String firstValue = fromMilisecondsToSecondsString(touchValue);
        String secondValue = firstResult.getText().toString();
        String thirdValue = secondResult.getText().toString();
        String fourthValue = thirdResult.getText().toString();

        firstResult.setText(firstValue);
        secondResult.setText(secondValue);
        thirdResult.setText(thirdValue);
        fourthResult.setText(fourthValue);
    }

    /**
     * Funcion que compara los tags recibidos con los esperados y corrige
     * los que tienen errores pero estan dentro de un porcentaje aceptable
     * ByWolf
     * */
    private String regexTagParser( String tag, String expectedTag){

        String regex = "(["+ expectedTag + "])";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(tag);

        int matches =0 ;
        while (matcher.find()) {
            matches++;
        }
        if((expectedTag.length()*0.6) <= matches && tag.length() >= (expectedTag.length()-1)){
            return expectedTag;
        }
        return tag;
    }

    /**
     * Funcion encargada de calcular el promedio, maximo y minimo del juego actual
     * */
    private void getFinalValues(ArrayList<Integer> values){
        int sum = 0;
        int correctTouchs = 0;
        max = 0;
        min = 99999;
        for (int i = 0; i<values.size(); i++){
            int newVal =  values.get(i);
            if(newVal > 0){
                correctTouchs++;
                sum += newVal;
                if (newVal > max){
                    max = newVal;
                }
                if(newVal < min){
                    min = newVal;
                }
            }else if(newVal == -200){
                correctTouchs++;
            }
        }
        totalTouchs = values.size();
        this.correctTouchs = correctTouchs;
        if(sum != 0){
            average = sum/correctTouchs;

        }else{
            average = 0;
            min = 0;
        }
    }

    /**
     * Funcion encargada de cambiar al fragmento de reconfiguracion
     * */
    private void transactionToReconfigure(){
        gameIsOn = false;
        Bundle bundle = new Bundle();
        bundle.putInt("average", average);
        bundle.putInt("min", min);
        bundle.putInt("max", max);
        bundle.putInt("correctTouchs", correctTouchs);
        bundle.putInt("totalTouchs", totalTouchs);
        ((MainActivity) getActivity()).deleteBluetoothMessage();

        FragmentTransaction transaction;
        ReconfigureFragment newFragment = new ReconfigureFragment();
        newFragment.setArguments(bundle);
        transaction =getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentHolderMenu, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    /**
     * Funcion encargada de darle formato correcto a los tiempos recibidos.
     * */
    private String fromMilisecondsToSecondsString(int milis){
        if(milis == -100) {
            return "x";
        }else if (milis == -200){
            return "✔";
        }

        float seconds = (float) milis / 1000;
        return String.format("%.3f s", seconds);
    }

    /**
     * Funcion encargada de controlar el mensaje recibido por bluetooth, asegurandose
     * que en el mismo no se encuentre la trama de resultados finales. ej:
     * ouch -100touch 213 results 100 100 -100 213 -> si esta trama es borrada sin controlar,
     * la aplicacion quedaria a la espera de la misma en la pantalla de results.
     * */
    private void controlBluetoothMessage(String bluetoothMessage) {
        int resultTagIndex = bluetoothMessage.indexOf("results");
        if (resultTagIndex >= 0) {
            String fixedMessage = bluetoothMessage.substring(resultTagIndex);
            ((MainActivity) getActivity()).fixBluetoothMessage(fixedMessage);
            Log.e("controlBluetoothMessage","MessageFixed: " + fixedMessage);
        }else {
            ((MainActivity) getActivity()).deleteBluetoothMessage();
        }
    }
}