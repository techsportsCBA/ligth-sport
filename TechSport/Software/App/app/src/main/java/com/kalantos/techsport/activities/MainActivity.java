package com.kalantos.techsport.activities;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.kalantos.techsport.BuildConfig;
import com.kalantos.techsport.R;
import com.kalantos.techsport.fragments.BluetoothFragment;
import com.kalantos.techsport.fragments.ConfigureFragment;
import com.kalantos.techsport.fragments.MenuFragment;
import com.kalantos.techsport.fragments.ResultsFragment;
import com.kalantos.techsport.utils.BluetoothConnectionService;
import com.kalantos.techsport.utils.Timer;
import java.nio.charset.Charset;
import com.bugfender.sdk.Bugfender;

public class MainActivity extends AppCompatActivity {

    String configuration;
    Bundle bundle;
    FragmentTransaction transaction;
    BluetoothConnectionService mBluetoothConnection;
    String bluetoothMessage = "";
    String playerName;
    boolean CheckBluetoothStateThread = false;
    boolean sendingConfig = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        onlineLoggerSetup();
        fullScreen();
        super.onCreate(savedInstanceState);
        bundle = new Bundle();
        setContentView(R.layout.activity_main);

        MenuFragment newFragmentMenu = new MenuFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentHolderMenu, newFragmentMenu);
        transaction.commit();

        BluetoothFragment newFragmentBluetooth = new BluetoothFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentHolderConectionBluetooth, newFragmentBluetooth);
        transaction.commit();
    }

    @Override
    public void onBackPressed(){
        if(getSupportFragmentManager().getBackStackEntryCount() == 0){
            finish();
            System.exit(0);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public void onStop(){
        CheckBluetoothStateThread = false;
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        super.onStop();
    }

    @Override
    public void onRestart(){
        checkThread();
        super.onRestart();
    }

    private void onlineLoggerSetup(){
        Bugfender.init(this, "1MRYwCTkmmrzvt7aJIyGFWLWAloT6PLV", BuildConfig.DEBUG);
        Bugfender.enableLogcatLogging();
        Bugfender.enableUIEventLogging(this.getApplication());
        Bugfender.enableCrashReporting();
    }

    public void checkThread(){
        if(CheckBluetoothStateThread){
            //"singleton" del hilo
            return;
        }
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                CheckBluetoothStateThread = true;
                while (CheckBluetoothStateThread) {
                    if(!sendingConfig) {
                        try {
                            byte[] bytes = "\0".getBytes(Charset.defaultCharset());
                            Boolean resultBTMessage = mBluetoothConnection.write(bytes);
                            if (!resultBTMessage) {
                                CheckBluetoothStateThread = false;
                                BluetoothFragment newFragment = new BluetoothFragment();
                                transaction = getSupportFragmentManager().beginTransaction();
                                transaction.replace(R.id.fragmentHolderConectionBluetooth, newFragment);
                                transaction.addToBackStack(null);
                                transaction.commit();
                            }
                        } catch (NullPointerException e) {
                            Log.e("CheckBluetoothThread", "Something went wrong! Exception catched");
                        }
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }else{
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        thread.start();
    }

    public String getBluetoothMessage(){
        return bluetoothMessage;
    }

    public void deleteBluetoothMessage(){
        bluetoothMessage = "";
    }

    public void fixBluetoothMessage(String message){
        bluetoothMessage = message;
    }

    public void displayToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void ConcatBluetoothMessage(String newPartOfMessage){
        bluetoothMessage += newPartOfMessage;
    }

    public void setBluetoothService(BluetoothConnectionService mBluetoothConnection){
        this.mBluetoothConnection = mBluetoothConnection;
    }

    public void cleanStackAndTransactionToConfigureFragment(View view){
        String name = getSupportFragmentManager().getBackStackEntryAt(0).getName();
        getSupportFragmentManager().popBackStack(name, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        transactionToConfigureFragment();
    }

    public void onClickTransactionToConfigureFragment(View view){
        transactionToConfigureFragment();
    }

    public void onClickTransactionToResultsFragment(View view){
        transactionToResultsFragment();
    }

    private void transactionToConfigureFragment(){
        ConfigureFragment newFragment = new ConfigureFragment();
        transaction =getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentHolderMenu, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void transactionToResultsFragment(){
        ResultsFragment newFragment = new ResultsFragment();
        newFragment.setArguments(bundle);
        transaction =getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentHolderMenu, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void cleanStackAndsendReconfiguration(View view){
        if(reSendConfiguration()) {
            String name = getSupportFragmentManager().getBackStackEntryAt(0).getName();
            getSupportFragmentManager().popBackStack(name, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            transactionToResultsFragment();
        }else {
            displayToast("Servidor no esta listo, espere luz verde");
        }
    }

    public boolean reSendConfiguration(){
        sendingConfig = true;
        deleteBluetoothMessage();
        byte[] bytes = configuration.getBytes(Charset.defaultCharset());
        mBluetoothConnection.write(bytes);
        int waitingAckTime = 0;
        while(waitingAckTime < 3){
            try {
                Thread.sleep(500);
                waitingAckTime++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(bluetoothMessage.equals("ack")){
                Log.d("MainActivity", configuration);
                sendingConfig = false;
                return true;
            }
        }
        Log.w("","ack not received");
        sendingConfig = false;
        return false;
    }

    public boolean sendConfiguration(String playerName, int touchs, int timeOn, int timeOff, String RGBColor, int gameMode, int gameBreak){
        sendingConfig = true;
        this.playerName = playerName;
        configuration = "config " + touchs + " " + timeOn * 1000 + " " + timeOff * 1000
                + " " + RGBColor + " " +gameMode + " " + gameBreak;
        bundle.putInt("timeOff", timeOff);
        bundle.putInt("touchs", touchs);
        deleteBluetoothMessage();
        byte[] bytes = configuration.getBytes(Charset.defaultCharset());
        mBluetoothConnection.write(bytes);
        int waitingAckTime = 0;
        while(waitingAckTime < 3){
            try {
                Thread.sleep(500);
                waitingAckTime++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(bluetoothMessage.equals("ack")){
                Log.d("MainActivity", configuration);
                sendingConfig = false;
                return true;
            }
        }
        Log.w("","ack not received");
        sendingConfig = false;
        return false;
    }

    private void fullScreen(){

        final View decorView = getWindow().getDecorView();
        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE;

        getWindow().getDecorView().setSystemUiVisibility(flags);
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
        {
            @Override
            public void onSystemUiVisibilityChange(int visibility)
            {
                if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                {
                    Thread thread=new Thread(new Timer());
                    thread.start();
                    try {
                        thread.join();
                    } catch (InterruptedException e) {
                        Log.e("AUTO-HIDE BAR", e.getMessage());
                    }

                    decorView.setSystemUiVisibility(flags);
                }
            }
        });
    }

}
