package com.kalantos.techsport.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.kalantos.techsport.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {


    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();
        fillConfigurations();
        Button saveAndExitButton = (Button) getActivity().findViewById(R.id.advancedsettings_saveandexit_button);
        saveAndExitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                saveAndBackToLastFragment();
            }
        });
    }

    public void fillConfigurations(){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("config_values",
                Context.MODE_PRIVATE);
        int timeOn = sharedPreferences.getInt("timeOn", 15);
        int timeOff = sharedPreferences.getInt("timeOff", 0);
        boolean gameBreak =  sharedPreferences.getBoolean("gameBreak", false);
        EditText editTimeOn = (EditText) getActivity().findViewById(R.id.advancedsettings_timeon_textview);
        EditText editTimeOff = (EditText) getActivity().findViewById(R.id.advancedsettings_timeoff_textview);
        Switch switchGameBreak = (Switch) getActivity().findViewById(R.id.advancedsettings_gamebreak_switch);
        editTimeOn.setText(timeOn + "");
        editTimeOff.setText(timeOff + "");
        switchGameBreak.setChecked(gameBreak);

    }
    private void saveAndBackToLastFragment(){

        EditText editTimeOff = (EditText) getActivity().findViewById(R.id.advancedsettings_timeoff_textview);
        EditText editTimeOn = (EditText) getActivity().findViewById(R.id.advancedsettings_timeon_textview);
        Switch switchGameBreak = (Switch) getActivity().findViewById(R.id.advancedsettings_gamebreak_switch);

        String timeOffString = editTimeOff.getText().toString();
        String timeOnString = editTimeOn.getText().toString();
        Boolean gameBreakValue = switchGameBreak.isChecked();

        saveBooleanConfiguration("gameBreak",gameBreakValue);
        if(!timeOffString.equals("")){
        int timeOff = Integer.parseInt(timeOffString);
            saveIntegerConfiguration("timeOff",timeOff);
        }
        if(!timeOffString.equals("")){
            int timeOn = Integer.parseInt(timeOnString);
            saveIntegerConfiguration("timeOn",timeOn);
        }
        getFragmentManager().popBackStackImmediate();
    }

    private void saveIntegerConfiguration(String tag, int value){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("config_values",
                Context.MODE_PRIVATE);
        sharedPreferences.edit().putInt(tag, value)
                .commit();
    }

    private void saveBooleanConfiguration(String tag, boolean value){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("config_values",
                Context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(tag, value)
                .commit();
    }

}
