package com.kalantos.techsport.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.kalantos.techsport.R;

public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_DURATION = 1500;
    private boolean imageVisible = true;
    private boolean splashIsActive = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        startTogglingImage();

        new Handler().postDelayed(new Runnable(){
            public void run(){
                splashIsActive = false;
                //Intent intent = new Intent(SplashActivity.this, BluetoothActivity.class);
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            };
        }, SPLASH_DURATION);
    }

    private void startTogglingImage(){
        Thread newThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(splashIsActive) {
                    if(imageVisible){
                        try {
                            Thread.sleep(350);
                            togleImage();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }else{
                        try {
                            Thread.sleep(200);
                            togleImage();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
        });
        newThread.start();
    }

    private void togleImage(){
        runOnUiThread(new Runnable() {
                          @Override
                          public void run() {

                              ImageView splashLogo = (ImageView) findViewById(R.id.splash_logo);
                              if(imageVisible){
                                  imageVisible = false;
                                  splashLogo.setVisibility(View.INVISIBLE);
                              }else{
                                  imageVisible = true;
                                  splashLogo.setVisibility(View.VISIBLE);
                              }
                          }
                      });

    }
}