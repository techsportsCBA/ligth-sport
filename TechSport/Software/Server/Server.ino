#include "Game.h"
#include "AppManager.h"
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <SoftwareSerial.h>

const char* ssid = "TechSport";
const char* password = "version01";
SoftwareSerial appBt(12, 14, false, 256);
Game game;
LedsManager leds;
AppManager app;


void setup() {
  Serial.begin(115200);
  checkBattery();
  //ESP.wdtDisable();
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  appBt.begin(115200);
  WiFi.mode(WIFI_AP);
  setupAccessPoint();
  while (!game.registerNodes()) {
    checkBattery();
    delay(1);
  }
  appBt.flush();
}

void loop() {
  if (app.receiveConfig(&appBt)) {
    game.setObserver(&appBt);
    int results[app.getTouchs()];
    if (game.newGame("Cristina", app.getMode(), app.getTouchs(), app.getColor(), app.getTimeOn(), app.getTimeOff(), app.getBreakGame(), results))
      app.sendResults(results, game.getTouchs(), &appBt);
    Serial.println("Participante: " + game.getName());
    Serial.println("Tiempo promedio: " + String(game.getAverageTime()) + "ms");
    Serial.println("Mejor Tiempo: " + String(game.getBestTime()) + "ms");
    Serial.println("Peor Tiempo: " + String(game.getWorstTime()) + "ms");
    delay(200);
  } else {
    checkBattery();
    delay(1);
  }
}

void setupAccessPoint() {
  Serial.println("** Iniciando Access Point **");
  Serial.println("- Iniciado con SSID: " + String(ssid));
  WiFi.softAP(ssid, password, 1, 0, NODES - 1);
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("- IP Address :");
  Serial.println(myIP);
}

void checkBattery(){  
  int batteryStatus= analogRead(A0);
  if(batteryStatus < 809){ //911 = 3.6v, 793 = 3.2v
    for (int i = 0; i < 4; i++){
      int batteryStatus= analogRead(A0);
      if(batteryStatus > 809){
        return;
      }
      delay(50);
    }
    leds.turnOnLowBatterySequence();
    ESP.deepSleep(0);
  }
}

