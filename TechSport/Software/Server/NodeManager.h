/**
 * Project Untitled
 */


#ifndef _NODEMANAGER_H
#define _NODEMANAGER_H
#include <Arduino.h>
#include "Node.h"
#include "UdpManager.h"
#include "LedsManager.h"
#include "SensorManager.h"
#include "SoftwareSerial.h"

#define DEBUG 1 //Activa o desactiva mensajes de debug

#if DEBUG
#define DEBUG_MSG(word) Serial.println(word);
#else
#define DEBUG_MSG(word);
#endif

#define NODES 5
#define TTL_MAX 5

class NodeManager {
public: 
    
    /**
     * Setea la configuración del nodo
     * @param node Número de nodo
     * @param trueOption Si es la opcion correcta o no
     * @param color Color a mostrar
     * @return 1 si se seteó, 0 si no se seteó
     */
    int setNode(int node, int trueOption, String color);

     /**
     * Chequea la conectividad de los nodos registrados
     * y actualiza la lista
     * @return cantidad de nodos conectados
     */
    int checkNodes();

    /**
     * Envía a los nodos conectados la información
     * registrada en la lista. Registra en la lista 
     * si se cayó algun nodo.
     * @return 1 si todos los nodos recibieron los datos
     * o 0 si hubo alguno que no lo recibió.
     */
    int sendData();

    /**
     * Espera que los nodos envíen el comando "register"
     * para registrarlos en la lista. Tiene un timeout 
     * que es waitToRegister.
     * @return cantidad de nodos registrados, 0 si no registró
     * ninguno
     */
    int registerNodes();

    /**
     * Espera que el usuario seleccione un nodo, y devuelve
     * el tiempo que se tardó en ms. Tambíen detecta si un nodo
     * se quiere registrar tardíamente, y lo registra.
     * @return tiempo en ms
     */
    int receiveData();
    
    /**
     * Envía a todos los nodos una señal (clear,loading,etc)
     * @return 1 si lo recibieron todos, 0 si hubo alguno que no la recibio
     */
    int sendSignal(String signal);
    

    /**
     * Setea el tiempo máximo(ms) para recibir una 
     * selección de nodo
     * @param time
     */
    void setMaxTimeReceive(int time);

    /**
     * Consulta si el nodo esta conectado
     * @param node nodo
     * @return 1 si esta conectado, 0 lo contrario
     */
    int isConnected(int node);

    /**
     * Indica si el nodo fue seleccionado como opción
     * verdadera
     * @param node nodo a consultar
     * @return 1 si es trueOption, 0 lo contrario
     */
    int isTrueOption(int node);

    /**
     * Retorna el color seteado del nodo
     * @param node nodo a consultar
     * @return String color del nodo
     */
    String getColor(int node);
 
private: 
    Node list[NODES];
    UdpManager udp;
    SensorManager sensor;
    LedsManager leds;
    int waitToRegister = 30000; //En ms
    int maxTimeReceive= 10000; //En ms
    String isAlive = "isAlive";
    int receives = 0;

   /**
    * Transforma un color de String a Int
    * Ej: "255 129 255" -> int[0]=255, int[1]=129, int[2]=255
    * @param str color en formato String
    * @param int array de destino
    */
    void colorToInt(String *str, int *integer);

    /**
     * Enciende el led de nodo maestro
     */
    void localSendData();

    /**
     * Toma datos del sensor del nodo maestro
     * @return 1 si la distancia es menor a 20cm, 0 lo contrario
     */
    int localReceiveData();

    /**
     * Envía una señal al nodo maestro
     */
    void localSendSignal(String signal);

    /**
     * Revisa si la ip ya esta registrada en la lista
     * @param ip ip a revisar
     * @return 1 si esta en la lista, 0 si no esta
     */
    int isRegistered(IPAddress *ip);
};

#endif //_NODEMANAGER_H
