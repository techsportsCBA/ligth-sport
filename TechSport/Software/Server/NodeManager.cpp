/**
   Project Untitled
*/


#include "NodeManager.h"

/**
   NodeManager implementation
*/

/**
  Setea la configuración del nodo
  @param node Número de nodo
  @param trueOption Si es la opcion correcta o no
  @param color Color a mostrar
  @return 1 si se seteó, 0 si no se seteó
*/
int NodeManager::setNode(int node, int trueOption, String color) {
  if (node < NODES && node >= 0) {
    if (list[node].isConnected) {
      list[node].color = color;
      list[node].trueOption = trueOption;
      return 1;
    } else if (!strcmp(list[node].ip.toString().c_str(), "0.0.0.0")) {
      DEBUG_MSG("[Node] Error: El nodo " + String(node) + " jamás se registró");
    } else {
      DEBUG_MSG("[Node] Error: Nodo " + String(node) + " sin conexión");
    }
  } else {
    DEBUG_MSG("[Node] Error: Nodo " + String(node) + " inválido");
  }
  return 0;
}

/**
  Chequea la conectividad de los nodos registrados
  y actualiza la lista
  @return cantidad de nodos conectados
*/
int NodeManager::checkNodes() {
  udp.clearBuffer();
  DEBUG_MSG("[Node] --- Chequeando Nodos Conectados ---");
  IPAddress ipNull;
  IPAddress ip[NODES - 1];
  String tag[NODES - 1];
  String data[NODES - 1];
  int isReceived[NODES - 1];
  int nodesConnected = 0;

  for ( int i = 0; i < NODES - 1; i++) {
    if (list[i].ip.toString() != "0.0.0.0") {
      ip[i] = list[i].ip;
      tag[i] = "isAlive";
      data[i] = " ";
      nodesConnected++;
      list[i].isConnected = 1;
    } else {
      tag[i] = "none";
    }
  }

  if (!udp.sendDataMultiCast(ip, tag, data, NODES - 1, isReceived)) {
    for (int i = 0; i < NODES - 1; i++) {
      if (!isReceived[i]) {
        DEBUG_MSG("[Node] Reenviando el mensaje al nodo " + String(i));
        for (int ttl = 0; ttl < TTL_MAX; ttl++) {
          if (udp.sendData(&ip[i], &tag[i], &data[i])) {
            DEBUG_MSG("[Node] --> √ Recibido!");
            DEBUG_MSG("[Node] Nodo " + String(i) + " conectado √");
            list[i].isConnected = 1;
            break;
          } else if ( ttl == TTL_MAX - 1) {
            list[i].isConnected = 0;
            DEBUG_MSG("[Node] --> X No Recibido!");
            DEBUG_MSG("[Node] Warning: Nodo " + String(i) + " desconectado X");
            nodesConnected--;
          }
        }
      } else if (isReceived[i] > 0) {
        DEBUG_MSG("[Node] Nodo " + String(i) + " conectado √");
      }
    }
  } else {
    DEBUG_MSG("[Node] Todos los nodos registrados estan conectados");
  }

  DEBUG_MSG("[Node] Se detectaron " + String(nodesConnected) + " nodos conectados");
  DEBUG_MSG("[Node] --- Fin Chequeo de Nodos Conectados ---");
  return nodesConnected;
}

/**
  Envía a los nodos conectados la información
  registrada en la lista. Registra en la lista
  si se cayó algun nodo.
  @return 1 si todos los nodos recibieron los datos
  o 0 si hubo alguno que no lo recibió.
*/
int NodeManager::sendData() {
  udp.clearBuffer();
  DEBUG_MSG("[Node] --- Enviando Datos ---");
  IPAddress ip[NODES - 1];
  String tag[NODES - 1];
  String data[NODES - 1];
  int isReceived[NODES - 1];
  int ret = 1;

  for ( int i = 0; i < NODES - 1; i++) {
    if (list[i].isConnected) {
      ip[i] = list[i].ip;
      tag[i] = "game";
      data[i] = String(list[i].trueOption) + " " + list[i].color;
    } else {
      tag[i] = "none";
    }
  }
  localSendData();
  if (!udp.sendDataMultiCast(ip, tag, data, NODES - 1, isReceived)) {
    for (int i = 0; i < NODES - 1; i++) {
      if (!isReceived[i]) {
        DEBUG_MSG("[Node] Reenviando el mensaje al nodo " + String(i));
        for (int ttl = 0; ttl < TTL_MAX; ttl++) {
          if (udp.sendData(&ip[i], &tag[i], &data[i])) {
            DEBUG_MSG("[Node] --> √ Recibido!");
            break;
          } else if ( ttl == TTL_MAX - 1) {
            list[i].isConnected = 0;
            DEBUG_MSG("[Node] --> X No Recibido!");
            DEBUG_MSG("[Node] Warning: Nodo " + String(i) + " desconectado X");
            ret = 0;
          }
        }
      }
    }
  } else {
    DEBUG_MSG("[Node] Todos los nodos conectados recibieron el mensaje");
  }

  DEBUG_MSG("[Node] --- Fin Envío de Datos ---");
  return ret;
}

/**
  Espera que los nodos envíen el comando "register"
  para registrarlos en la lista. Tiene un timeout
  que es waitToRegister.
  @return cantidad de nodos registrados, 0 si no registró
  ninguno
*/
int NodeManager::registerNodes() {
  udp.clearBuffer();
  int ret = 0;
  if (udp.open())
  {
    int i = 0;
    IPAddress ip;
    String tag;
    String data;

    unsigned long start = millis();

    DEBUG_MSG("[Node] --- Inicio Registración de Nodos ---");
    while (i < NODES - 1 && (millis() - start) < waitToRegister) {
      if (udp.receiveData(&ip, &tag, &data)) {
        if (!strcmp("register", tag.c_str())) {
          if (!isRegistered(&ip)) {
            list[i].ip = ip;
            list[i].color = "0 0 0";
            list[i].trueOption = 0;
            list[i].isConnected = 1;
            DEBUG_MSG("[Node] Se registró el nodo " + String(i) + " con IP: " + ip.toString());
            i++;
            start = millis();
          } else {
            DEBUG_MSG("[Node] Warning: La IP: " + ip.toString() + " ya se encuentra registrada");
          }
        }
        else {
          DEBUG_MSG("[Node] Warning: Esperaba un register y recibí un " + tag);
        }
      } else {
        leds.turnLedsOnCircle(80, 0, 0);
      }
    }

    list[NODES - 1].ip = WiFi.softAPIP();
    list[NODES - 1].color = "0 0 0";
    list[NODES - 1].trueOption = 0;
    list[NODES - 1].isConnected = 1;

    if (i == 0) {
      DEBUG_MSG("[Node] Error: No se registró ningun nodo");
      leds.turnLedsOn(80, 0, 0);
      delay(5000);
      leds.turnLedsOff();
      list[NODES - 1].isConnected = 0;
      return 0;
    }

    DEBUG_MSG("[Node] Se registró el nodo maestro " + String(NODES - 1) + " con IP: " + WiFi.softAPIP().toString());
    ret = i;
    if (i == NODES - 1) {
      leds.turnLedsOn(0, 80, 0);
      delay(5000);
      leds.turnLedsOff();
    } else if (i < NODES - 1) {
      DEBUG_MSG("[Node] Warning: Se registraron solo " + String(i + 1) + " nodos por TimeOut");
      leds.turnLedsOn(40, 40, 0);
      delay(5000);
      leds.turnLedsOff();
      while ( i < NODES - 1) {
        list[i].isConnected = 0;
        i++;
      }
    }
    DEBUG_MSG("[Node] --- Fin Registración de Nodos ---");
    return ret;
  }

  return ret;
}

/**
  Espera que el usuario seleccione un nodo, y devuelve
  el tiempo que se tardó en ms. Tambíen detecta si un nodo
  se quiere registrar tardíamente, y lo registra.
  @return tiempo en ms
*/
int NodeManager::receiveData() {
  udp.clearBuffer();
  int selected = 0;
  IPAddress ip;
  String tag;
  String data;
  unsigned long start;
  int counter = -100;

  DEBUG_MSG("[Node] --- Inicio Recepción de Nodo Seleccionado ---");
  start = millis();

  while (millis() - start < maxTimeReceive) {
    if (udp.receiveData(&ip, &tag, &data)) {
      if (!strcmp("selected", tag.c_str())) {  // Detecta si se seleccionó un nodo de los esclavos
        for (int i = 0; i < NODES - 1; i++) {
          if (ip == list[i].ip) {
            if (list[i].trueOption) {
              DEBUG_MSG("[Node] Nodo " + String(i) + " seleccionado! - Opción Correcta √");
              selected = 1;
              counter = millis() - start;
              break;
            } else {
              DEBUG_MSG("[Node] Nodo " + String(i) + " seleccionado! - Opción Incorrecta X");
              selected = 1;
              counter = -50;
              break;
            }
          }
        }
      } else if (!strcmp("register", tag.c_str())) {  // Detecta si un nodo se quiere registrar
        if (isRegistered(&ip)) {        // Busca si esta en la lista
          for (int i = 0; i < NODES - 1; i++) {
            if (ip == list[i].ip) {
              list[i].color = "0 0 0";
              list[i].trueOption = 0;
              list[i].isConnected = 0;
              DEBUG_MSG("[Node] Se reconectó nodo " + String(i) + " con IP: " + ip.toString());
              break;
            }
          }
        } else {
          for (int i = 0; i < NODES - 1; i++) {
            if (!strcmp(list[i].ip.toString().c_str(), "0.0.0.0")) {
              list[i].ip = ip;
              list[i].color = "0 0 0";
              list[i].trueOption = 0;
              list[i].isConnected = 0;
              DEBUG_MSG("[Node] Se registró tardíamente el nodo " + String(i) + " con IP: " + ip.toString());
              break;
            }
          }
        }
      } else {
        DEBUG_MSG("[Node] Warning: Esperaba un selected y recibí un " + tag);
      }
    } else if (localReceiveData()) {   // Detecta si se seleccionó el nodo maestro
      if (list[NODES - 1].trueOption) {
        DEBUG_MSG("[Node] Nodo " + String(NODES - 1) + " seleccionado! - Opción Correcta √");
        selected = 1;
        counter = millis() - start;
        break;
      } else {
        DEBUG_MSG("[Node] Nodo " + String(NODES - 1) + " seleccionado! - Opción Incorrecta X");
        selected = 1;
        counter = -50;
        break;
      }
    }
    if (selected) {
      break;
    }
    delay(1);
  }



  DEBUG_MSG("[Node] Tiempo esperado: " + String(counter) + " ms");
  DEBUG_MSG("[Node] --- Fin Recepción de Nodo Seleccionado ---");
  sendSignal("clear");
  receives++;
  /*
    if ( strcmp(observer.toString().c_str(), "0.0.0.0")) {
    String tag = "touch";
    //String data = String(receives) + " " + String (counter);
    String data = String (counter);
    udp.sendData(&observer, &tag, &data, 6780);
    }
  */
  
  return counter;
}

/**
  Setea el tiempo máximo(ms) para recibir una
  selección de nodo
  @param time
*/
void NodeManager::setMaxTimeReceive(int time) {
  maxTimeReceive = time;
}

/**
  Envía a todos los nodos una señal (clear,loading,etc)
  @return 1 si lo recibieron todos, 0 si hubo alguno que no la recibio
*/
int NodeManager::sendSignal(String signal) {
  udp.clearBuffer();
  DEBUG_MSG("[Node] --- Enviando señal \"" + signal + "\" ---");
  IPAddress ip[NODES - 1];
  String tag[NODES - 1];
  String data[NODES - 1];
  int isReceived[NODES - 1];
  int ret = 1;

  for ( int i = 0; i < NODES - 1; i++) {
    if (list[i].isConnected) {
      ip[i] = list[i].ip;
      tag[i] = signal;
      data[i] = " ";
    } else {
      tag[i] = "none";
    }
  }
  localSendSignal(signal);
  if (!udp.sendDataMultiCast(ip, tag, data, NODES - 1, isReceived)) {
    for (int i = 0; i < NODES - 1; i++) {
      if (!isReceived[i]) {
        DEBUG_MSG("[Node] Reenviando el mensaje al nodo " + String(i));
        for (int ttl = 0; ttl < TTL_MAX; ttl++) {
          if (udp.sendData(&ip[i], &tag[i], &data[i])) {
            DEBUG_MSG("[Node] --> √ Recibido!");
            break;
          } else if ( ttl == TTL_MAX - 1) {
            list[i].isConnected = 0;
            DEBUG_MSG("[Node] --> X No Recibido!");
            DEBUG_MSG("[Node] Warning: Nodo " + String(i) + " desconectado X");
            ret = 0;
          }
        }
      }
    }
  }
  if (signal == "loading") {
    delay(2000);
    String clear = "clear";
    localSendSignal(clear);
  }
  DEBUG_MSG("[Node] --- Fin Envío de señal ---");
  return ret;
}

/**
  Envía una señal al nodo maestro
*/
void NodeManager::localSendSignal(String signal) {
  if (!strcmp(signal.c_str(), "clear")) {
    leds.turnLedsOff();
  } else if (!strcmp(signal.c_str(), "loading")) {
    //leds.turnLedsLoading();
    leds.turnLedsOn(0, 80, 0);
  }

}

/**
  Enciende el led de nodo maestro
*/
void NodeManager::localSendData() {
  int color[3];
  colorToInt(&list[NODES - 1].color, color);
  leds.turnLedsOn(color[0], color[1], color[2]);
}

/**
  Toma datos del sensor del nodo maestro
  @return 1 si la distancia es menor a 20cm, 0 lo contrario
*/
int NodeManager::localReceiveData() {
  if ( list[NODES - 1].color != "0 0 0") {
    if ( sensor.getDistance() < 20) {
      return 1;
    }
  }
  return 0;
}

/**
  Revisa si la ip ya esta registrada en la lista
  @param ip ip a revisar
  @return 1 si esta en la lista, 0 si no esta
*/
int NodeManager::isRegistered(IPAddress * ip) {
  for ( int i = 0; i < NODES - 1; i++) {
    if (*ip == list[i].ip) {
      return 1;
    }
  }
  return 0;
}

/**
  Transforma un color de String a Int
  Ej: "255 129 255" -> int[0]=255, int[1]=129, int[2]=255
  @param str color en formato String
  @param int array de destino
*/
void NodeManager::colorToInt(String *str, int *integer) {
  String string = *str;
  int j = 0;
  int m = 0;
  for (int i = 0; i < string.length(); i++) {
    if (string.substring(i, i + 1) == " " ) {
      integer[j] = string.substring(m, i).toInt();
      m = i + 1;
      j++;
    }
  }
  integer[j] = string.substring(m, string.length()).toInt();
}

/**
  Consulta si el nodo esta conectado
  @param node nodo
  @return 1 si esta conectado, 0 lo contrario
*/
int NodeManager::isConnected(int node) {
  if (node >= 0 && node < NODES)
    return list[node].isConnected;
  return 0;
}

/**
  Indica si el nodo fue seleccionado como opción
  verdadera
  @param node nodo a consultar
  @return 1 si es trueOption, 0 lo contrario
*/
int NodeManager::isTrueOption(int node) {
  if (node > NODES - 1 || node < 0) {
    DEBUG_MSG("[Node] Error: Nodo consultado inválido");
    return 0;
  } else if (list[node].trueOption) {
    return 1;
  }
  return 0;
}

/**
  Retorna el color seteado del nodo
  @param node nodo a consultar
  @return String color del nodo
*/
String NodeManager::getColor(int node) {
  if (node > NODES - 1 || node < 0) {
    DEBUG_MSG("[Node] Error: Nodo consultado inválido");
    return "none";
  } 
  return list[node].color;
}

