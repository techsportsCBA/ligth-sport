/**
   Project Untitled
*/


#include "Game.h"

/**
   Game implementation
*/


/**
  Inicia una nueva ronda
  @param player Nombre del participante
  @param mode tipo de juego
  @param times cantidad de touchs de la ronda
  @param trueColor color de opcion correcta(por defecto azul)
  @return 1 si salio todo bien, 0 si se desconectaron todos los nodos
*/
int Game::newGame(String name, int mode, int times, String trueColor, int timeOn, int timeOff, int breakGame, int* results) {
  int list[NODES];
  int length;
  int (Game::*gameMode)(int *list, int length); // Puntero a la función del modo seleccionado
  nodeList.setMaxTimeReceive(timeOn);
  this->timeOff = timeOff;

  DEBUG_MSG("[Game] --- Iniciando Nueva Ronda ---");
  int nodes = nodeList.checkNodes();
  DEBUG_MSG("[Game] Se encontraron " + String(nodes) + " nodos conectados");
  if (nodes) {
    if ( times > 0) {
      if (trueColor.length())
        this->trueColor = trueColor;
      player.name = name;
      touchs = times;
      this->breakGame = breakGame;

      switch (mode) {
        case SIMPLE:
          gameMode = &Game::simple;
          break;
        case SIMPLE_ERRORS:
          gameMode = &Game::simpleWithErrors;
          break;
        case STATIC_SV:
          gameMode = &Game::staticServer;
          break;
        case RANDOM_LIGHTS:
          gameMode = &Game::randomLights;
          break;
        case ALL_LIGHTS:
          gameMode = &Game::allLights;
          break;
        case SIMON_SAYS:
          gameMode = &Game::simonSays;
          break;
        default:
          DEBUG_MSG("[Game] Error: Modo desconocido");
          return 0;
      }

      nodeList.sendSignal("loading");

      length = getConnectedList(list); //Obtiene la lista de nodos conectados

      isNewGame = 1;

      for (int i = 0; i < times; i++) {
        results[i] = (this->*gameMode)(list, length);
        if (results[i] == -1) {
          if (length > getConnectedList(list)) {
            length = getConnectedList(list);
            i--;
            DEBUG_MSG("[Game] Warning: Se desconectó al menos un nodo");
          } else if (!getConnectedList(list)) {
            DEBUG_MSG("[Game] Error: Se desconectaron todos los nodos");
            return 0;
          } else {
            return 0;
          }
        } else {
          if (results[i] == -50)
            results[i] = -100;
          if ( observer != NULL) {
            String packet = "touch ";
            packet += String(results[i]);
            delay(1);
            observer->write(packet.c_str());
          }
          DEBUG_MSG("[Game] Tiempo " + String(i) + " = " + String(results[i]) + "ms");
          if ( this->breakGame && (results[i] == -100)) {
            touchs = i;
            i = times;
          }
          delay(timeOff);
        }
      }
      calculateTimes(results, times);

    } else {
      DEBUG_MSG("[Game] Error: La cantidad de juegos de la ronda debe ser mayor a 0");
      return 0;
    }
  } else {
    DEBUG_MSG("[Game] Error: No es posible iniciar el juego sin nodos conectados");
    return 0;
  }
  lastSv = 0;
  DEBUG_MSG("[Game] --- Ronda Finalizada ---");
  return 1;
}

/**
  Retorna el mejor tiempo del participante
  @return tiempo
*/
int Game::getBestTime() {
  return player.bestTime;
}

/**
  Retorna el peor tiempo del participante
  @return tiempo
*/
int Game::getWorstTime() {
  return player.worstTime;
}

/**
  Retorna el tiempo promedio del participante
  @return tiempo
*/
int Game::getAverageTime() {
  return player.averageTime;
}

/**
  Retorna el nombre del participante
  @return tiempo
*/
String Game::getName() {
  return player.name;
}

/**
  Retorna la cantidad de touchs
  @return touchs
*/
int Game::getTouchs() {
  return touchs;
}

/**
  Registra los nodos en la lista
  @return cantidad de nodos registrados
*/
int Game::registerNodes() {
  DEBUG_MSG("[Game] Registrando nodos");
  int nodes = nodeList.registerNodes();
  if(nodes){
    DEBUG_MSG("[Game] Se registraron " + String(nodes) + " nodos");
  } else {
    DEBUG_MSG("[Game] No se registro ningun nodo");
  }
  return nodes;
}

/**
  Envía informacion a los nodos de la próxima jugada y recibe
  el tiempo que tardó el usuario en responder. La modalidad
  es de una sola luz encendida.
  @param list lista mapeada de los nodos conectados
  @param length cantidad de nodos conectados
  @return 1 si salió todo bien, -1 si se desconectó algún nodo
*/
int Game::simple(int *list, int length) {
  if (length <= 0) {
    DEBUG_MSG("[Game] Error: Tamaño de lista inválido ");
    return -1;
  }
  int rand = getRandom(length);
  for (int i = 0; i < length; i++) {
    nodeList.setNode(list[i], 0, "0 0 0");
  }
  nodeList.setNode(list[rand], 1, trueColor);

  if (nodeList.sendData()) {        //Si se envió bien
    return nodeList.receiveData(); // Recibir el tiempo que tardo el usuario
  } else {
    nodeList.sendSignal("clear");
  }
  return -1;
}

/**
  Envía informacion a los nodos de la próxima jugada y recibe
  el tiempo que tardó el usuario en responder. La modalidad
  es de varias luces encendidas con colores aleatorios.
  @param list lista mapeada de los nodos conectados
  @param length cantidad de nodos conectados
  @return 1 si salió todo bien, -1 si se desconectó algún nodo
*/
int Game::randomLights(int *list, int length) {
  if (length <= 0) {
    DEBUG_MSG("[Game] Error: Tamaño de lista inválido ");
    return -1;
  } else if (!setRainbow(list, length)) {  //Obtengo los nodos ya seteados con sus colores
    return -1;
  }

  int rand = random(2);  //Probabilidad del 50% de que le toque apagarse
  String ledOff = "0 0 0";

  for (int i = 0; i < length; i++) {
    if (nodeList.isTrueOption(list[i]) || rand) { //Si es opc verdadera o le toco 1, deja el color
      rand = random(2);                           //que le toco
      continue;
    } else {
      nodeList.setNode(list[i], 0, ledOff); //Sino se apaga
      rand = random(2);
    }
  }

  if (nodeList.sendData()) {        //Si se envió bien
    return nodeList.receiveData(); // Recibir el tiempo que tardo el usuario
  } else {
    nodeList.sendSignal("clear");
  }
  return -1;
}

/**
  Envía informacion a los nodos de la próxima jugada y recibe
  el tiempo que tardó el usuario en responder. La modalidad
  es de todas las luces encendidas con colores aleatorios.
  @param list lista mapeada de los nodos conectados
  @param length cantidad de nodos conectados
  @return 1 si salió todo bien, -1 si se desconectó algún nodo
*/
int Game::allLights(int *list, int length) {
  if (length <= 0) {
    DEBUG_MSG("[Game] Error: Tamaño de lista inválido ");
    return -1;
  } else if (!setRainbow(list, length)) {
    return -1;
  }

  if (nodeList.sendData()) {        //Si se envió bien
    return nodeList.receiveData(); // Recibir el tiempo que tardo el usuario
  } else {
    nodeList.sendSignal("clear");
  }
  return -1;
}

/**
  Envía informacion a los nodos de la próxima jugada y recibe
  el tiempo que tardó el usuario en responder. La modalidad
  es una luz encendida con probabilidad de que encienda un color
  erróneo.
  @param list lista mapeada de los nodos conectados
  @param length cantidad de nodos conectados
  @return 1 si salió todo bien, -1 si se desconectó algún nodo
*/
int Game::simpleWithErrors(int *list, int length) {
  if (length <= 0) {
    DEBUG_MSG("[Game] Error: Tamaño de lista inválido ");
    return -1;
  }
  static int errorCounter;
  static int counter;

  if (isNewGame) {
    errorCounter = 0;
    counter = 0;
    isNewGame = 0;
  }

  int colors = sizeof(colorList) / sizeof(colorList[0]);
  int randLigth = random(colors);               // Color aleatorio de la lista de colores
  while (checkColorLikeness(randLigth)) {
    randLigth = random(colors);
  }
  int rand = getRandom(length);                 // Número aleatorio de nodo (sin repetir el anterior)
  int probability = random(touchs * 10);                 // Probabilidad de que toque el error

  for (int i = 0; i < length; i++) {
    nodeList.setNode(list[i], 0, "0 0 0");
  }

  if ((errorCounter == 0) && (counter == touchs - 1)) {
    probability = 10 * touchs;
  }

  if (probability < (touchs * 8) ) {
    nodeList.setNode(list[rand], 1, trueColor);
  } else {
    nodeList.setNode(list[rand], 0, String(colorList[randLigth][0]) + " " + String(colorList[randLigth][1]) +
                     " " + String(colorList[randLigth][2]));
    errorCounter++;
  }

  if (nodeList.sendData()) {        //Si se envió bien
    int result = nodeList.receiveData(); // Recibir el tiempo que tardo el usuario
    if (probability < (touchs * 8) ) {
      return result;
    } else if (result == -100) {
      return -200;
    } else {
      return -100;
    }
    counter++;
  } else {
    nodeList.sendSignal("clear");
  }
  return -1;
}

/**
  Envía informacion a los nodos de la próxima jugada y recibe
  el tiempo que tardó el usuario en responder. La modalidad
  es una luz encendida. Primero enciende el servidor, y luego
  algún cliente aleatorio.
  @param list lista mapeada de los nodos conectados
  @param length cantidad de nodos conectados
  @return 1 si salió todo bien, -1 si se desconectó algún nodo
*/
int Game::staticServer(int *list, int length) {
  if (length <= 0) {
    DEBUG_MSG("[Game] Error: Tamaño de lista inválido ");
    return -1;
  }
  int rand;
  for (int i = 0; i < length; i++) {
    nodeList.setNode(list[i], 0, "0 0 0");
  }
  if (!lastSv) {
    nodeList.setNode(list[length - 1], 1, trueColor);
    lastSv = 1;
  } else {
    rand = getRandom(length - 1);
    nodeList.setNode(list[rand], 1, trueColor);
    lastSv = 0;
  }

  if (nodeList.sendData()) {        //Si se envió bien
    return nodeList.receiveData(); // Recibir el tiempo que tardo el usuario
  } else {
    nodeList.sendSignal("clear");
  }
  return -1;
}

/**
  Envía informacion a los nodos de la próxima jugada y recibe
  el tiempo que tardó el usuario en responder. La modalidad
  es simon dice. El servidor cumple la función de simon.
  @param list lista mapeada de los nodos conectados
  @param length cantidad de nodos conectados
  @return 1 si salió todo bien, -1 si se desconectó algún nodo
*/
int Game::simonSays(int *list, int length) {
  if (length <= 0) {
    DEBUG_MSG("[Game] Error: Tamaño de lista inválido ");
    return -1;
  } else if (!setRainbow(list, length)) {
    return -1;
  }
  int rand = getRandom2(length - 1);

  for (int i = 0; i < length; i++) { // Elimino la opción verdadera impuesta por setRainbow
    if (nodeList.isTrueOption(list[i])) {
      nodeList.setNode(list[i], 0, nodeList.getColor(list[i]));
    }
  }

  nodeList.setNode(list[rand], 1, nodeList.getColor(list[rand]));  //Elijo una aleatoria menos la del server
  nodeList.setNode(list[length - 1], 0, nodeList.getColor(list[rand])); //Le pongo el mismo color al server

  if (nodeList.sendData()) {        //Si se envió bien
    return nodeList.receiveData(); // Recibir el tiempo que tardo el usuario
  } else {
    nodeList.sendSignal("clear");
  }
  return -1;
}


/**
  Mapea los nodos conectados en una lista
  @param list lista mapeada de los nodos conectados
  @return cantidad de nodos conectados
*/
int Game::getConnectedList(int *list) {
  int j = 0;
  int nodesOn = 0;

  for (int i = 0; i < NODES; i++) { //Mapeo los nodos conectados en una lista
    if (nodeList.isConnected(i)) {
      nodesOn++;
      list[j] = i;
      j++;
    }
  }
  if (nodesOn > 1) {
    return nodesOn;
  }
  return 0;
}

/**
  Calcula los tiempos del participante y los registra
  en la clase privada Person player
  @param measures array de mediciones de tiempos
  @param times cantidas de touchs de la ronda
*/
void Game::calculateTimes(int *measures, int times) {
  player.bestTime = measures[0];
  player.worstTime = 0;
  for (int i = 0; i < times ; i++) {
    player.averageTime += measures[i];
    if (player.worstTime < measures[i])
      player.worstTime = measures[i];
    if (player.bestTime > measures[i])
      player.bestTime = measures[i];
  }
  player.averageTime /= times;
}

/**
  Establece una ip observadora, que reciba los datos
  recibidos de los nodos(tiempo de touch)
  @param ip ip del observador
*/
void Game::setObserver(SoftwareSerial *serial) {
  this->observer = serial;
}

/**
  Detecta similaridad entre un color de la lista
  colorList y el trueColor
  @param colorListIndex indice de la lista
  @return 1 si son similares, 0 lo contrario
*/
int Game::checkColorLikeness(int colorListIndex) {

  int sum;
  int sumTrue;
  int trueColor[3];
  //sum = colorList[colorListIndex][0];
  //sum += colorList[colorListIndex][1];
  //sum += colorList[colorListIndex][2];
  colorToInt(&(this->trueColor), trueColor);
  //sumTrue = trueColor[0] + trueColor[1] + trueColor[2];
  //if ( sum - sumTrue >= 80 || sum - sumTrue <= -80) {
  //  return 0;
  //} else if ( sum - sumTrue == 0) {
  if (colorList[colorListIndex][0] != trueColor[0] ||
      colorList[colorListIndex][1] != trueColor[1] ||
      colorList[colorListIndex][2] != trueColor[2]) {
    return 0;
  }
  //}
  return 1;

  //return 0;
}

/**
  Transforma un color de String a Int
  Ej: "255 129 255" -> int[0]=255, int[1]=129, int[2]=255
  @param str color en formato String
  @param int array de destino
*/
void Game::colorToInt(String *str, int *integer) {
  String string = *str;
  int j = 0;
  int m = 0;
  for (int i = 0; i < string.length(); i++) {
    if (string.substring(i, i + 1) == " " ) {
      integer[j] = string.substring(m, i).toInt();
      m = i + 1;
      j++;
    }
  }
  integer[j] = string.substring(m, string.length()).toInt();
}

/**
  Retorna un número aleatorio entre 0 y max
  @param max máximo número
  @return numero aleatorio
*/
int Game::getRandom(int max) {
  int rand = random(max);
  while (rand == lastRandom) {
    rand = random(max);
  }
  lastRandom = rand;
  return rand;
}

/**
  Retorna un número aleatorio entre 0 y max
  @param max máximo número
  @return numero aleatorio
*/
int Game::getRandom2(int max) {
  int rand = random(max);
  while (rand == lastRandom2) {
    rand = random(max);
  }
  lastRandom2 = rand;
  return rand;
}

int Game::setRainbow(int *list, int length) {
  int wDog = 0;
  int colorSelector[length];
  int rows = sizeof(colorList) / sizeof(colorList[0]);

  for (int i = 0; i < 50; i++) {
    colorSelector[0] = random(rows);
    if (!checkColorLikeness(colorSelector[0])) {
      int n = colorSelector[0];
      nodeList.setNode(list[0], 0, String(colorList[n][0]) + " " + String(colorList[n][1]) + " " + String(colorList[n][2]));
      break;
    }
    else if (i > 48) {
      DEBUG_MSG("[Game] Error: True Color coincide con varios colores de la base 1");
      return 0;
    }
  }

  for (int i = 1; i < length; i++) {
    colorSelector[i] = random(rows);
    for (int j = 0; j < i; j++) {
      if (colorSelector[j] == colorSelector[i] || checkColorLikeness(colorSelector[i])) {
        i--;
        wDog++;
        if (wDog > 50) {
          DEBUG_MSG("[Game] Error: True Color coincide con varios colores de la base");
          return 0;
        }
        break;
      } else if (j + 1 == i) {
        wDog = 0;
        int n = colorSelector[i];
        nodeList.setNode(list[i], 0, String(colorList[n][0]) + " " + String(colorList[n][1]) + " " + String(colorList[n][2]));
      }
    }
  }
  int rand = getRandom(length);
  nodeList.setNode(list[rand], 1, trueColor);
  return 1;
}


