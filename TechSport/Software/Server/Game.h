/**
 * Project Untitled
 */


#ifndef _GAME_H
#define _GAME_H

#include "NodeManager.h"
#include "Person.h"
#include <Arduino.h>
#include "SoftwareSerial.h"

// Tipos de Juego
#define SIMPLE 0
#define SIMPLE_ERRORS 1
#define ALL_LIGHTS 2
#define RANDOM_LIGHTS 3
#define STATIC_SV 4
#define SIMON_SAYS 5


#define DEBUG 1 //Activa o desactiva mensajes de debug

#if DEBUG
#define DEBUG_MSG(word) Serial.println(word);
#else
#define DEBUG_MSG(word);
#endif

#define TTL_MAX 5


class Game {
public: 
    
    /**
     * Inicia una nueva ronda
     * @param player Nombre del participante
     * @param mode tipo de juego
     * @param times cantidad de touchs de la ronda
     * @param trueColor color de opcion correcta(por defecto azul)
     * @param timeOn tiempo de encendido del led
     * @param timeOff tiempo de apagado del led
     * @param breakGame selecciona si el juego finaliza ante un error(1) o
     *                  de lo contrario con un 0 no finaliza.
     * @return 1 si salio todo bien, 0 si se desconectaron todos los nodos
     */
    int newGame(String player, int mode, int times, String trueColor, int timeOn, int timeOff, int breakGame, int *results);

    /**
     * Retorna el mejor tiempo del participante
     * @return tiempo
     */
    int getBestTime();

    /**
     * Retorna el peor tiempo del participante
     * @return tiempo
     */
    int getWorstTime();

    /**
     * Retorna el tiempo promedio del participante
     * @return tiempo
     */
    int getAverageTime();

    /**
     * Retorna el nombre del participante
     * @return tiempo
     */
    String getName();

    /**
     * Registra los nodos en la lista
     * @return cantidad de nodos registrados
     */
    int registerNodes();
    
    /**
     * Establece un observador, que reciba los datos
     * recibidos de los nodos(tiempo de touch)
     * @param serial canal uart de comunicación
     */
    void setObserver(SoftwareSerial *serial);

    /**
     * Retorna la cantidad de touchs
     * @return touchs
     */
    int getTouchs();
    
private: 
    NodeManager nodeList;
    Person player;
    String trueColor= "0 0 80";
    int timeOff = 500;
    int touchs;
    int breakGame;
    int colorList[7][3] = { {80,   0,   0}, {0,   80,   0}, {0,     0, 80},
                            {40, 40,   0}, {40,   0, 40}, {0,   40, 40},
                            {30, 30, 30} };
    int lastRandom = -1; //Ultimo número aleatorio
    int lastRandom2 = -1; //Ultimo número aleatorio
    int lastSv = 0;      //Flag para conocer si el server se encendió anteriormente
    int isNewGame = 0;
    SoftwareSerial *observer;

    /**
     * Envía informacion a los nodos de la próxima jugada y recibe
     * el tiempo que tardó el usuario en responder. La modalidad
     * es de una sola luz encendida.
     * @param list lista mapeada de los nodos conectados
     * @param length cantidad de nodos conectados
     * @return 1 si salió todo bien, -1 si se desconectó algún nodo
     */
    int simple(int *list, int length);

    /**
     * Envía informacion a los nodos de la próxima jugada y recibe
     * el tiempo que tardó el usuario en responder. La modalidad
     * es de varias luces encendidas con colores aleatorios.
     * @param list lista mapeada de los nodos conectados
     * @param length cantidad de nodos conectados
     * @return 1 si salió todo bien, -1 si se desconectó algún nodo
     */
    int randomLights(int *list, int length);

    /**
     * Envía informacion a los nodos de la próxima jugada y recibe
     * el tiempo que tardó el usuario en responder. La modalidad
     * es de todas las luces encendidas con colores aleatorios.
     * @param list lista mapeada de los nodos conectados
     * @param length cantidad de nodos conectados
     * @return 1 si salió todo bien, -1 si se desconectó algún nodo
     */
    int allLights(int *list, int length);

    /**
     * Envía informacion a los nodos de la próxima jugada y recibe
     * el tiempo que tardó el usuario en responder. La modalidad
     * es una luz encendida con probabilidad de que encienda un color 
     * erróneo.
     * @param list lista mapeada de los nodos conectados
     * @param length cantidad de nodos conectados
     * @return 1 si salió todo bien, -1 si se desconectó algún nodo
     */
    int simpleWithErrors(int *list, int length);

    /**
     * Envía informacion a los nodos de la próxima jugada y recibe
     * el tiempo que tardó el usuario en responder. La modalidad
     * es una luz encendida. Primero enciende el servidor, y luego
     * algún cliente aleatorio.
     * @param list lista mapeada de los nodos conectados
     * @param length cantidad de nodos conectados
     * @return 1 si salió todo bien, -1 si se desconectó algún nodo
     */
    int staticServer(int *list, int length);

    /**
     * Envía informacion a los nodos de la próxima jugada y recibe
     * el tiempo que tardó el usuario en responder. La modalidad
     * es simon dice. El servidor cumple la función de simon.
     * @param list lista mapeada de los nodos conectados
     * @param length cantidad de nodos conectados
     * @return 1 si salió todo bien, -1 si se desconectó algún nodo
     */
    int simonSays(int *list, int length);

    /**
     * Mapea los nodos conectados en una lista
     * @param list lista mapeada de los nodos conectados
     * @return cantidad de nodos conectados
     */
    int getConnectedList(int *list);

    /**
     * Calcula los tiempos del participante y los registra
     * en la clase privada Person player
     * @param measures array de mediciones de tiempos
     * @param times cantidas de touchs de la ronda
     */
    void calculateTimes(int *measures, int times);   

    /**
    * Transforma un color de String a Int
    * Ej: "255 129 255" -> int[0]=255, int[1]=129, int[2]=255
    * @param str color en formato String
    * @param int array de destino
    */
    void colorToInt(String *str, int *integer);

    /**
    * Detecta similaridad entre un color de la lista
    * colorList y el trueColor
    * @param colorListIndex indice de la lista
    * @return 1 si son similares, 0 lo contrario
    */
    int checkColorLikeness(int colorListIndex);

    /**
    * Retorna un número aleatorio entre 0 y max
    * @param max máximo número
    * @return numero aleatorio
    */
    int getRandom(int max);

    /**
    * Retorna un número aleatorio entre 0 y max
    * @param max máximo número
    * @return numero aleatorio
    */
    int getRandom2(int max);

    /**
     * Setea cada nodo con un color diferente
     * @param list lista mapeada de los nodos conectados
     * @param length cantidad de nodos conectados
     * @return 1 si salió todo bien, 0 si el TrueColor concide con
     * varios colores con la ColorList
     */
    int setRainbow(int *list, int length);
};

#endif //_GAME_H
