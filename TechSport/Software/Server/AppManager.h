/**
 * Project Untitled
 */


#ifndef _APPMANAGER_H
#define _APPMANAGER_H
#include "UdpManager.h"
#include "LedsManager.h"
#include <ESP8266WiFi.h>
#include <SoftwareSerial.h>

#define DEBUG 1 //Activa o desactiva mensajes de debug


#if DEBUG
#define DEBUG_MSG(word) Serial.println(word);
#else
#define DEBUG_MSG(word);
#endif

#define TTL_MAX 10
#define CFG_PARAM 8

#define DEFAULT_TIMEON 10000
#define DEFAULT_TIMEOFF 200
#define DEFAULT_MODE 0
#define DEFAULT_TOUCHS 20
#define DEFAULT_BREAKGAME 0



class AppManager {
public: 

    /**
     * Constructor de la clase
     */
    AppManager();
    
    /**
     * Envía a la aplicación una matriz de valores
     * @param results matriz a enviar
     * @param length tamaño
     * @return 1 si la aplicación lo recibió, 0 lo contrario
     */
    int sendResults(int *results, int length, SoftwareSerial *appBt);

    /**
     * Espera que la aplicación envíe la palabra de configuración
     * "config" con los parámetros correspondientes:
     * config touchs timeOn timeOff R G B(color)
     * @return 1 si el mensaje recibido es coherente, 0 lo contrario
     */
    int receiveConfig(Stream *appBt);
    
    /**
     * Busca caracter por caracter en el software serial y arma un string
     * con lo recibido
     * @return String con el mensaje recibido
     */
    String readBluetoothSerial(Stream *appBt);

    /**
     * Retorna la IP de la aplicación
     * @return ip
     */
    IPAddress getIp();
    
    /**
     * Retorna el color elegido
     * @return color
     */
    String getColor();

    /**
     * Retorna el timeOn elegido
     * @return timeOn
     */
    int getTimeOn();

    /**
     * Retorna el timeOff elegido
     * @return timeOff
     */
    int getTimeOff();

    /**
     * Retorna el modo elegido
     * @return modo
     */
    int getMode();

    /**
     * Retorna la cantidad de touchs elegida
     * @return touchs
     */
    int getTouchs();

    /**
     * Retorna la modalidad break
     * @return break
     */
    int getBreakGame();

private: 
    UdpManager udp;
    LedsManager leds;
    IPAddress ip;
    int appPort = 6780;
    //--- Valores por defecto ---//
    String color = "0 0 80";
    int timeOn = DEFAULT_TIMEON;
    int timeOff= DEFAULT_TIMEOFF;
    int mode = DEFAULT_MODE;
    int touchs = DEFAULT_TOUCHS;
    int breakGame = DEFAULT_BREAKGAME;
    //---------------------------//

    /**
     * Parsea los parametros recibidos de la app,
     * y los guarda en las variables de la clase.
     * @param data String de datos recibidos
     * @return 1 si los datos son coherentes, 0 lo contrario
     */
    int parseData(String* data);
    
};

#endif //_APPMANAGER_H
