/**
   Project Untitled
*/


#include "AppManager.h"

/**
   AppManager implementation
*/
/**
  Constructor de la clase
*/
AppManager::AppManager() {
}

/**
  Espera que la aplicación envíe la palabra de configuración
  "config" con los parámetros correspondientes:
  config touchs timeOn timeOff R G B(color)
  @return 1 si el mensaje recibido es coherente, 0 lo contrario
*/
int AppManager::receiveConfig(Stream *appBt) {
  IPAddress ipTemp;
  String temp;
  String tag = "";
  String data;
  char c;
  unsigned long start;
  DEBUG_MSG("[App] Esperando configuración");
  while ( tag != "config") {
    while (!appBt->available() > 0) {
      leds.turnLedsOnCircleStep(0, 100, 0);
      delay(1);
    }
    delay(100);
    temp = readBluetoothSerial(appBt);
    if ( temp.length() > (5 + (CFG_PARAM * 2))) {
      data = temp.c_str() + 7;
      tag = temp.substring(0, 6);
    }
    yield();
  }
  DEBUG_MSG("[App] TAG: " + tag);
  DEBUG_MSG("[App] DATA: " + data);
  DEBUG_MSG("[App] Configuración recibida");
  ip = ipTemp;
  appBt->write("ack");
  return parseData(&data);
}

/**
  Busca caracter por caracter en el software serial y arma un string
  con lo recibido
  @return String con el mensaje recibido
*/
String AppManager::readBluetoothSerial(Stream *appBt) {
  String message = "";
  while (appBt->available() > 0) {
    char newChar = appBt->read();
    if (newChar != '\0') {
      message += newChar;
    }
    delay(1);
  }
  return message;
}

/**
  Envía a la aplicación una matriz de valores
  @param results matriz a enviar
  @param length tamaño
  @return 1 si la aplicación lo recibió, 0 lo contrario
*/
int AppManager::sendResults(int *results, int length, SoftwareSerial *appBt) {
  //udp.clearBuffer();
  String data = "results ";
  String temp = "";
  int timeOut = 0;

  for (int i = 0; i < length - 1; i++) {
    data += String(results[i]) + " ";
  }
  if (length)
    data += String(results[length - 1]);

  DEBUG_MSG("[App] Enviando datos a la aplicación");

  delay(1000);
  appBt->write(data.c_str());

  /*
    while (temp != "ack") {
    if (appBt->available() > 0) {
      delay(100);
      temp = appBt->readString();
    } else {
      leds.turnLedsOnCircleStep(0, 0, 100);
      timeOut++;
      if (timeOut > 8) {
        appBt->write(data.c_str());
        timeOut = 0;
      }
    }
    }
  */

  DEBUG_MSG("[App] Datos Enviados");
  return 1;
}

/**
  Retorna la IP de la aplicación
  @return ip
*/
IPAddress AppManager::getIp() {
  return ip;
}

/**
  Retorna el color elegido
  @return color
*/
String AppManager::getColor() {
  return color;
}

/**
  Retorna el timeOn elegido
  @return timeOn
*/
int AppManager::getTimeOn() {
  return timeOn;
}

/**
  Retorna el timeOff elegido
  @return timeOff
*/
int AppManager::getTimeOff() {
  return timeOff;
}

/**
  Retorna el modo elegido
  @return modo
*/
int AppManager::getMode() {
  return mode;
}

/**
  Retorna la cantidad de touchs elegida
  @return touchs
*/
int AppManager::getTouchs() {
  return touchs;
}

/**
  Retorna el estado de la modalidad break
  @return break
*/
int AppManager::getBreakGame() {
  return breakGame;
}

int AppManager::parseData(String* data) {
  int dataTemp[CFG_PARAM];
  int index = 0;
  int j = 0;
  int ret = 0;
  for (int i = 0; i < data->length(); i++) {
    if (data->substring(i, i + 1) == " ") {
      dataTemp[index] = data->substring(j, i).toInt();
      j = i + 1;
      index++;
      if (index == CFG_PARAM - 1) {
        dataTemp[index] = data->substring(j).toInt();
        ret = 1;
        break;
      }
    }
  }
  for (int i = 0; i < CFG_PARAM; i++) {
    if (dataTemp[i] < 0) { // Si el parametro enviado es 0 o menor, usa el por defecto
      continue;
    }
    switch (i) {
      case 0:
        touchs = dataTemp[i];
        if (!touchs)
          touchs = DEFAULT_TOUCHS;
        break;
      case 1:

        timeOn = dataTemp[i];
        if (!timeOn)
          timeOn = DEFAULT_TIMEON;
        break;
      case 2:

        timeOff = dataTemp[i];
        if (!timeOff)
          timeOff = DEFAULT_TIMEOFF;
        break;
      case 3:
        if ( !dataTemp[i] && !dataTemp[i + 1] && !dataTemp[i + 2]) {
          i + 2;
        } else {
          color = String(dataTemp[i]) + " " + String(dataTemp[i + 1]) + " " + String(dataTemp[i + 2]);
          i + 2;
        }
        break;
      case 6:
        mode = dataTemp[i];
        if (!mode)
          mode = DEFAULT_MODE;
        break;
      case 7:
        breakGame = dataTemp[i];
        if (!breakGame)
          breakGame = DEFAULT_BREAKGAME;
        break;
    }
  }
  DEBUG_MSG("[App] Touchs: " + String(touchs));
  DEBUG_MSG("[App] Time On: " + String(timeOn));
  DEBUG_MSG("[App] Time Off: " + String(timeOff));
  DEBUG_MSG("[App] True Color: " + color);
  DEBUG_MSG("[App] Mode: " + String(mode));
  DEBUG_MSG("[App] BreakGame: " + String(breakGame));
  return ret;
}

